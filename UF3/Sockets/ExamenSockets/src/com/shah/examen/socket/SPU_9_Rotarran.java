package com.shah.examen.socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class SPU_9_Rotarran {

    public static String fi = "";

    public static void main(String[] args) {

        ServerSocket nauSocket = null;
        Socket releSocket;
        boolean endComunication = false;

        try {
            System.out.println("SPU-9 Rotarran - INICI");
            nauSocket = new ServerSocket(IKSRotarranConstants.PORT_NAU);

            while (!endComunication) {

                System.out.println("SPU-9 Rotarran: esperant la connexió del relé");
                releSocket = nauSocket.accept();

                procesarComunicacionsAmbRele(releSocket);

                if (fi.equalsIgnoreCase("n")) {
                    endComunication = true;
                }
            }
            System.out.println("SPU-9 Rotarran - FI");


            //Cerrar el socket nau
            if ((nauSocket != null) && (!nauSocket.isClosed())) {
                nauSocket.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void procesarComunicacionsAmbRele(Socket releSocket) {

        BufferedReader inputNau;
        PrintWriter outputNau;
        String messageToSend = "";
        String messageReceived = "";

        try {
            inputNau = new BufferedReader(new InputStreamReader(releSocket.getInputStream()));
            outputNau = new PrintWriter(new OutputStreamWriter(releSocket.getOutputStream()), true);

            messageReceived = inputNau.readLine();

            if (messageReceived.equals("DEMANAR_PERMIS_CONNEXIO")) {
                String answar = respondreAPermisConnexio(outputNau);

                if (answar.equalsIgnoreCase("s")) {

                    outputNau.println("PERMIS_CONNEXIO_CONCEDIT");
                    outputNau.flush();

                    String opcion = showMenu();

                    for (int i = 0; i < 2; i++) {
                        switch (opcion){
                            case "1":
                                //Opcion1
                                inicialitzarEspionatge(releSocket, inputNau, outputNau);
                                break;
                            case "2":
                                //Opcion2
                                inicialitzarAutodestruccio(releSocket, inputNau, outputNau);
                                break;
                        }
                        if (i<1){
                            opcion = showMenu();
                        }
                    }
                    closeClient(releSocket);
                }

            } else {
                closeClient(releSocket);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void inicialitzarAutodestruccio(Socket releSocket, BufferedReader inputNau, PrintWriter outputNau) throws IOException {
        String messageToSend = "";
        String messageReceived = "";

        messageToSend = "SONDA_INICIAR_AUTODESTRUCCIO";
        System.out.println("Nau --> relé --> sonda: " + messageToSend);
        outputNau.println(messageToSend);
        outputNau.flush();

        messageReceived = inputNau.readLine();
        System.out.println("Sonda --> relé --> nau: " + messageReceived);
    }

    private static void inicialitzarEspionatge(Socket releSocket, BufferedReader inputNau, PrintWriter outputNau) throws IOException {
        String messageToSend = "";
        String messageReceived = "";

        messageToSend = "SONDA_INICI_ESPIAR";
        System.out.println("Nau --> relé --> sonda: " + messageToSend);
        outputNau.println(messageToSend);
        outputNau.flush();

        messageReceived = inputNau.readLine();
        System.out.println("Sonda --> relé --> nau: " + messageReceived);
    }

    private static String respondreAPermisConnexio(PrintWriter permission) {
        String answer;
        System.out.println("Permets la connexió del client? (S/N) ");
        answer = getScanner().next();

        if (answer.equalsIgnoreCase("S")) {
            permission.println("PERMIS_CONNEXIO_CONCEDIT");
        } else {
            permission.println("PERMIS_CONNEXIO_DENEGAT");
        }
        permission.flush();
        fi = answer;

        return answer;
    }

    public static Scanner getScanner() {
        Scanner sc = new Scanner(System.in);
        return sc;
    }

    private static void closeClient(Socket clientSocket) {
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    public static String showMenu() {
        StringBuilder menu = new StringBuilder();
        menu.append(System.lineSeparator());
        menu.append("Orbita 10");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("1. Que la sonda inicii l'espionatge");
        menu.append(System.lineSeparator());
        menu.append("2. Autodestruccio de la sonda");
        menu.append(System.lineSeparator());
        System.out.println(menu.toString());

        String opcion = getScanner().next();

        return opcion;
    }
}
