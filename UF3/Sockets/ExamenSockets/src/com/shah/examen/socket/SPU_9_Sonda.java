package com.shah.examen.socket;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class SPU_9_Sonda {

    public static void main(String[] args) {

        Socket sondaSocket;
        BufferedReader input;
        PrintWriter output;

        try {
            System.out.println("SPU-9 Sonda - INICI");
            sondaSocket = new Socket(InetAddress.getByName(IKSRotarranConstants.ADDRESS_SPU9), IKSRotarranConstants.PORT_RELE);
            input = new BufferedReader(new InputStreamReader(sondaSocket.getInputStream()));
            output = new PrintWriter(new OutputStreamWriter(sondaSocket.getOutputStream()), true);

            connectarAmbElRele(sondaSocket, input, output);

            //Cerrar socket
            closeClient(sondaSocket);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void connectarAmbElRele(Socket sondaSocket, BufferedReader input, PrintWriter output) {
        String messageToSend = "DEMANAR_PERMIS_CONNEXIO";
        String messageReceived = "";

        try {
            System.out.println("Sonda: esperant permís de connexió de la nau a través del relé: ");
            output.println(messageToSend);
            output.flush();

            messageReceived = input.readLine();
            System.out.println("Nau --> rele --> sonda:" + messageReceived);

            if (messageReceived.equals("PERMIS_CONNEXIO_DENEGAT")){
                System.out.println("SPU-9 Sonda: No tenim permís de la nau per a connectar-nos. Tanquem el socket.");
                System.out.println("SPU-9 Sonda: FI");
            }

            if (messageReceived.equals("PERMIS_CONNEXIO_CONCEDIT")){
                comunicarseAmbElRele(sondaSocket, input, output);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void comunicarseAmbElRele(Socket sondaSocket, BufferedReader input, PrintWriter output) {

        String messageSend;
        String messageRecived;

        try {
            messageRecived = input.readLine();
            System.out.println("Nau --> Relé --> sonda: "+ messageRecived);
            Thread.sleep(5000);

            messageSend = "SONDA_FI_ESPIAR";
            output.println(messageSend);
            output.flush();



            messageRecived = input.readLine();
            System.out.println("Nau --> Relé --> sonda: "+ messageRecived);
            messageSend = "SONDA_FI_AUTODESTRUCCIO";
            output.println(messageSend);
            output.flush();

            System.out.println("SPU-9 Sonda: FI");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static Scanner getScanner() {
        Scanner sc = new Scanner(System.in);
        return sc;
    }

    private static void closeClient(Socket clientSocket) {
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
