package com.shah.examen.socket;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class SPU_9_Rele_comunicacions {

    public static void main(String[] args) {

        ServerSocket releServerSocket;
        Socket sondaSocket;

        try {
            System.out.println("SPU-9 Relé de comunicacions - INICI");
            releServerSocket = new ServerSocket(IKSRotarranConstants.PORT_RELE);

            System.out.println("SPU-9 Rotarran: esperant la connexió del sonda...");
            sondaSocket = releServerSocket.accept();

            procesarComunicacionsEntreNauISonda(sondaSocket);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void procesarComunicacionsEntreNauISonda(Socket sondaSocket) {
        BufferedReader inputSonda;
        PrintWriter outputSonda;

        try {
            inputSonda = new BufferedReader(new InputStreamReader(sondaSocket.getInputStream()));
            outputSonda = new PrintWriter(new OutputStreamWriter(sondaSocket.getOutputStream()), true);

            connectarAmbLaNau(inputSonda, outputSonda);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void connectarAmbLaNau(BufferedReader inputSonda, PrintWriter outputSonda) {
        Socket nauSocket;
        BufferedReader inputNau;
        PrintWriter outputNau;
        String messageSendToNau = "";
        String messageReceivedFromNau = "";
        String messageSendToSonda = "";
        String messageReceivedFromSonda = "";

        try {
            nauSocket = new Socket(InetAddress.getByName(IKSRotarranConstants.ADDRESS_SPU9), IKSRotarranConstants.PORT_NAU);
            inputNau = new BufferedReader(new InputStreamReader(nauSocket.getInputStream()));
            outputNau = new PrintWriter(new OutputStreamWriter(nauSocket.getOutputStream()), true);

            //Leer el mensaje de sonda
            messageReceivedFromSonda = inputSonda.readLine();
            System.out.println("Relé: demanem permís a la nau per a connectar-nos.");

            //Enviar el mensaje de sonda a nau
            messageSendToNau = messageReceivedFromSonda;
            outputNau.println(messageSendToNau);
            outputNau.flush();

            System.out.println("Mensaje de permisos enviado");


            //Leer el mensaje de nau
            messageReceivedFromNau = inputNau.readLine();
            System.out.println("Nau --> rele: " + messageReceivedFromNau);

            //Enviar el mensaje de nau a sonda
            messageSendToSonda = messageReceivedFromNau;

            outputSonda.println(messageSendToSonda);
            outputSonda.flush();


            if (messageReceivedFromNau.equals("PERMIS_CONNEXIO_DENEGAT")){
                System.out.println("Relé: No tenim permís de la nau per a connectar-nos. Tanquem el socket.");
                System.out.println("SPU-9 Sonda: FI");
            }

            if (messageReceivedFromNau.equals("PERMIS_CONNEXIO_CONCEDIT")){
                boolean fiComunicacio = false;
                while (!fiComunicacio){

                    //Recibir el mensaje nau
                    messageReceivedFromNau = inputNau.readLine();
                    System.out.println("Nau --> Relé: "  + messageReceivedFromNau);

                    //Enviar mensaje de nau a sonda
                    messageSendToSonda = messageReceivedFromNau;
                    System.out.println("Relé --> Sonda: "  + messageSendToSonda);
                    outputSonda.println(messageSendToSonda);
                    outputSonda.flush();

                    //Recibir mensaje de sonda
                    messageReceivedFromSonda = inputSonda.readLine();
                    System.out.println("Sonda --> Relé: "  + messageReceivedFromSonda);

                    //Enviar el mensaje de sonda a nau
                    messageSendToNau = messageReceivedFromSonda;
                    System.out.println("Relé --> Nau: "  + messageSendToNau);
                    outputNau.println(messageSendToNau);
                    outputNau.flush();

                    if (messageSendToNau == null){
                        fiComunicacio = true;
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void closeClient(Socket clientSocket) {
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
