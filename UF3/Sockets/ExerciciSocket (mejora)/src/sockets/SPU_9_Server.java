package sockets;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class SPU_9_Server {

    static final int PORT = 6000;
    private static boolean endComunication = false;

    /*
    Els identificadors del tipus de transmissió que es farà són:
        DEMANAR_PERMIS_CONNEXIO: La envía el cliente al server para Pedirle Permiso de conexión.
        PERMIS_CONNEXIO_CONCEDIT: La envía el server al cliente para indicarle que tiene permiso para aconnectarse.
        RETORN_CONTROL: Es para pasarle el control de las comunicaciones al otro.
        INICIALITZAR_CHAT: Es para indicar que queremos chatear.
        FINALITZAR_CHAT: Es para indicar que queremos terminar de chatear.
        INICIALITZAR_FTP: Es para indicar que queremos enviar un archivo a través de FTP.
     */

    public static void main(String[] args) {
        System.out.println("El servidor se ha iniciado.");
        escoltar();
        System.out.println("El servidor ha sido cerrado!");
    }

    private static void escoltar() {
        boolean endComunication = false;
        ServerSocket serverSocket = null;
        Socket clientSocket = null;

        try {
            serverSocket = new ServerSocket(PORT);

            while (!endComunication) {
                //Servidor esuchando para que algun cliente se connecte
                //El cliente es quien inicializa la comunicación.
                clientSocket = serverSocket.accept();
                System.out.println("Cliente connectado al servidor.");

                //Cuando algun cliente se connecta llamamos al método procesarComunicacionsAmbClient.
                procesarComunicacionsAmbClient(clientSocket);
                tancarClient(clientSocket);
                System.out.println("SERVER.escoltar(): el client s'ha desconnectat, esperant a que es connecti un altre client...");
            }

            //Cerramos el serverSocket
            if ((serverSocket != null) && (!serverSocket.isClosed())) {
                serverSocket.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void procesarComunicacionsAmbClient(Socket clientSocket) {
        boolean endComunication = false;
        String clientMessage = "";
        BufferedReader inputByClientSocketInCharacters = null;
        PrintWriter outputByClientSocketInCharacters = null;

        try {
            inputByClientSocketInCharacters = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            outputByClientSocketInCharacters = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            do {
                clientMessage = inputByClientSocketInCharacters.readLine();
                endComunication = procesarMissatgesDelClient(clientSocket, inputByClientSocketInCharacters, outputByClientSocketInCharacters, clientMessage);
            } while (!endComunication);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void tancarClient(Socket clientSocket) {
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static boolean procesarMissatgesDelClient(Socket clientSocket, BufferedReader inputByClientSocketInCharacters, PrintWriter outputByClientSocketInCharacters, String clientMessage) {
        boolean endComunication = false;
        boolean showMenu = false;
        String messageType = "";
        int selectedOption;

        //Comprobar si el mensaje del cliente es nulo o no.
        if (clientMessage != null) {
            messageType = clientMessage;
        } else {
            messageType = "El mensaje es nulo";
            endComunication = true;
        }

        if (messageType.equals("DEMANAR_PERMIS_CONNEXIO")) {
            respondreAPermisConnexio(outputByClientSocketInCharacters);
        }

        //Mostrar el menu del servidor
        if (messageType.equals("RETORN_CONTROL")) {
            showMenu = true;
        }

        if (messageType.equals("INICIALITZAR_CHAT")) {
            //Chat
        }

        if (messageType.equals("INICIALITZAR_FTP")) {
            //ftp
        }


        if (showMenu) {
            do {
                showMenu();
                selectedOption = getScanner().nextInt();
                switch (selectedOption) {
                    case 0:
                    case 50:
                        endComunication = true;
                        showMenu = false;
                        break;
                    case 1:
                        retornControl(true, outputByClientSocketInCharacters);
                        showMenu = false;
                        break;
                    case 2:
                        //chat(true, readClientMessageInCaracters, sendMessageToClientInCaracters);
                        System.out.println();
                        break;
                    case 3:
                        //ftp(true, clientSocket, readClientMessageInCaracters, sendMessageToClientInCaracters);
                        break;
                    default:
                        System.out.println("COMANDA NO RECONEGUDA");
                }
            } while ((!endComunication) && (showMenu) && (!(selectedOption == 50 || selectedOption == 0)));
        }

        return endComunication;
    }

    //Comprobar si el usuario tiene permisos para conectar-se o no.
    private static void respondreAPermisConnexio(PrintWriter outputByClientSocketInCharacters) {

        System.out.print("Permets la connexió del client? (S/N) ");
        String permission = getScanner().next();

        if (permission.equalsIgnoreCase("s")) {
            outputByClientSocketInCharacters.println("PERMIS_CONNEXIO_CONCEDIT");
        } else {
            outputByClientSocketInCharacters.println("PERMIS_CONNEXIO_DENEGAT");
        }
        outputByClientSocketInCharacters.flush();
    }

    private static void retornControl(boolean startComunication, PrintWriter outputByClientSocketInCharacters) {
        outputByClientSocketInCharacters.println("RETORN_CONTROL");
        outputByClientSocketInCharacters.flush();
    }

    public static String showMenu() {
        StringBuilder menu = new StringBuilder();
        menu.append(System.lineSeparator());
        menu.append("SPU-9 SERVER");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("0. Desconnectar-se del CLIENT");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("1. Retornar el control de les comunicacions al CLIENT");
        menu.append(System.lineSeparator());
        menu.append("2. CHAT");
        menu.append(System.lineSeparator());
        menu.append("3. FTP");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("50. Tancar el sistema");
        menu.append(System.getProperty("line.separator"));
        return menu.toString();
    }

    public static Scanner getScanner() {
        Scanner sc = new Scanner(System.in);
        return sc;
    }
}
