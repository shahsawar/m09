package sockets;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class SPU_9_Client {

    private static final String ADDRESS = "127.0.0.1";
    private static final int PORT = 6000;

    public static void main(String[] args) {
        System.out.println("Cliente Connectado.");
        connectarAmbElServer(ADDRESS, PORT);
        System.out.println("Cliente se ha desconnectado.");
    }

    private static void connectarAmbElServer(String address, int port) {
        Socket clientSocket;
        BufferedReader inputForSocket_in_caracters;
        PrintWriter outPutToSocket_in_caracters;
        String serverMessage = "";
        boolean endComunications = false;

        try {
            clientSocket = new Socket(InetAddress.getByName(address), port);
            inputForSocket_in_caracters = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            outPutToSocket_in_caracters = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);

            //Envío de la petición de conexión. Inicializando la comunicación con el server.
            System.out.println("Client: demanem permís al server per a connectar-nos.");
            outPutToSocket_in_caracters.println("DEMANAR_PERMIS_CONNEXIO");
            outPutToSocket_in_caracters.flush();

            //Leer la respuesta del servidor
            serverMessage = inputForSocket_in_caracters.readLine();

            if (serverMessage.equals("PERMIS_CONNEXIO_CONCEDIT")) {

            } else {
                System.out.println("Client: NO tenim permís del server per a connectar-nos. Tanquem el socket.");
            }

            tancarSocket(clientSocket);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // Cerrar el socket del cliente
    private static void tancarSocket(Socket clientSocket) {
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static String showMenu() {
        StringBuilder menu = new StringBuilder();
        menu.append(System.lineSeparator());
        menu.append("SPU-9 CLIENT");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("0. Desconnectar-se del SERVIDOR");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("1. Retornar el control de les comunicacions al SERVIDOR");
        menu.append(System.lineSeparator());
        menu.append("2. CHAT");
        menu.append(System.lineSeparator());
        menu.append("3. FTP");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("50. Tancar el sistema");
        menu.append(System.getProperty("line.separator"));
        return menu.toString();
    }

    public static Scanner getScanner() {
        Scanner sc = new Scanner(System.in);
        return sc;
    }

}
