package com.shah.exerciciSockets;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * @autor shah
 */
public class SPU_9_server {

    private static final int PORT = 6000;
    private static boolean endComunication = false;
    private static boolean showMenu = true;

    public static void main(String[] args) {
        System.out.println("Servidor Iniciado.");
        escoltar();
        System.out.println("El servidor se ha detenido.");
    }

    private static void escoltar() {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);

            while (!endComunication) {
                System.out.println("Servidor esperando que el cliente se conecte... ");
                Socket clientSocket = serverSocket.accept();
                System.out.println("Cliente connectado");

                procesarComunicacionsAmbClient(clientSocket);
                closeClient(clientSocket);

                System.out.println("Cliente desconectado. Servidor esperando que otro cliente se conecte...");
            }
            if ((serverSocket != null) && (!serverSocket.isClosed())) {
                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void closeClient(Socket clientSocket) {
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void procesarComunicacionsAmbClient(Socket clientSocket) {
        Boolean endComunication = false;

        try {
            //Abrimos entrada y salida para caracteres en el socket.
            BufferedReader readClientMessageInCaracters = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter sendMessageToClientInCaracters = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            do {
                String clientMessage = readClientMessageInCaracters.readLine();
                endComunication = procesarMissatgesDelClient(clientSocket, clientMessage, readClientMessageInCaracters, sendMessageToClientInCaracters);
            } while (!endComunication);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static Boolean procesarMissatgesDelClient(Socket clientSocket, String clientMessage, BufferedReader readClientMessageInCaracters, PrintWriter sendMessageToClientInCaracters) {
        int option;
        boolean endConnectionWithClient = false;
        String messageOption = "";

        showMenu = false;

        if (clientMessage == null) {
            messageOption = "El mensaje es nulo";
            endConnectionWithClient = true;
        } else {
            messageOption = clientMessage;

            switch (messageOption) {
                case "DEMANAR_PERMIS_CONNEXIO":
                    respondreAPermisConnexio(sendMessageToClientInCaracters);
                    break;
                case "RETORN_CONTROL":
                    showMenu = true;
                case "INICIALITZAR_CHAT":
                    chat(false, readClientMessageInCaracters, sendMessageToClientInCaracters);
                    System.out.println();
                    break;
                case "INICIALITZAR_FTP":
                    ftp(false, clientSocket, readClientMessageInCaracters, sendMessageToClientInCaracters);
                    System.out.println();
                    break;
            }
        }

        if (showMenu) {
            do {
                System.out.println(showMenu());
                option = getScanner().nextInt();
                switch (option) {
                    case 0:
                    case 50:
                        endConnectionWithClient = false;
                        showMenu = false;
                        break;
                    case 1:
                        retornControl(true, sendMessageToClientInCaracters);
                        showMenu = false;
                        break;
                    case 2:
                        chat(true, readClientMessageInCaracters, sendMessageToClientInCaracters);
                        System.out.println();
                        break;
                    case 3:
                        ftp(true, clientSocket, readClientMessageInCaracters, sendMessageToClientInCaracters);
                        break;
                    default:
                        System.out.println("COMANDA NO RECONEGUDA");
                }
            } while ((showMenu) && (option != 50) && (!endConnectionWithClient));
        }
        return endConnectionWithClient;
    }

    private static void ftp(boolean startOperation, Socket clientSocket, BufferedReader readClientMessageInCaracters, PrintWriter sendMessageToClientInCaracters) {

        String messageRecived = "";
        String fileToSend = "serverFile.txt";
        String fileToRecived;
        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        OutputStream sendToSocket_in_flux = null;
        InputStream readFromSocket_in_flux = null;

        if (startOperation) {
            try {
                sendMessageToClientInCaracters.println("INICIALITZAR_FTP");
                sendMessageToClientInCaracters.flush();

                messageRecived = readClientMessageInCaracters.readLine();
                System.out.println("ServerFtp: missatge rebut del client " + messageRecived);

                sendMessageToClientInCaracters.println(fileToSend);
                sendMessageToClientInCaracters.flush();

                File file = new File("serverFile.txt");
                sendMessageToClientInCaracters.println(file.length());
                sendMessageToClientInCaracters.flush();

                byte[] bytesArray = new byte[1024 * 16];

                //Sleep for half second
                Thread.sleep(500);

                bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                sendToSocket_in_flux = clientSocket.getOutputStream();

                System.out.println("FTP transmetent... " + fileToSend + "(" + file.length() + " bytes)");
                int readLength = -1;
                int i = 1;
                int numBytesSend = 0;

                while ((readLength = bufferedInputStream.read(bytesArray)) > 0) {
                    sendToSocket_in_flux.write(bytesArray, 0, readLength);
                    sendToSocket_in_flux.flush();

                    numBytesSend = numBytesSend + readLength;
                    System.out.println(i + ": Enviat " + readLength + " bytes del fitxer.");
                    i++;
                }
                System.out.println("Fi d'enviatment de fitxer. Enviats " + numBytesSend + " bytes.");

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                messageRecived = readClientMessageInCaracters.readLine();
                System.out.println("SERVER.fpt(): missatge rebut del client: " + messageRecived);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {

            try {
                // Retornem al client perquè pugui començar el ftp.
                retornControl(false, sendMessageToClientInCaracters);

                // Rebem el nom del fitxer:
                fileToRecived = readClientMessageInCaracters.readLine();
                fileToRecived = fileToRecived;

                int tamanyFitxer = Integer.parseInt(readClientMessageInCaracters.readLine());

                System.out.println("Nom del fitxer a rebre: " + fileToRecived);
                System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");

                System.out.println("FTP server rebent...");

                // Rebem el fitxer (podríem rebre diversos blocs de tamany 1024*16 bytes):
                byte[] mybytearray = new byte[1024 * 16];
                readFromSocket_in_flux = clientSocket.getInputStream();
                bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(fileToRecived));
                int count;
                int i = 1;
                int numBytesRebuts = 0;

                while ((count = readFromSocket_in_flux.read(mybytearray)) != -1) { //&& (mybytearray[count-1] != '\0')) {
                    bufferedOutputStream.write(mybytearray, 0, count);

                    numBytesRebuts = numBytesRebuts + count;

                    System.out.println(i + ": Rebut " + count + " bytes del fitxer, mybytearray.length = " + mybytearray.length + ", numBytesRebuts = " + numBytesRebuts);
                    i++;

                    if (numBytesRebuts == tamanyFitxer) {
                        break;
                    }
                }

                bufferedOutputStream.close();

                System.out.println("Nom del fitxer a rebre: " + fileToRecived);
                System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");
                System.out.println("Fi de recepció de fitxer. Rebuts " + numBytesRebuts + " bytes.");

                sendMessageToClientInCaracters.println("FINALITZAT_FTP. Rebut " + numBytesRebuts + " bytes.");
                sendMessageToClientInCaracters.flush();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private static void chat(boolean startOperation, BufferedReader readClientMessageInCaracters, PrintWriter sendMessageToClientInCaracters) {
        String messageToSend = "";
        String messageRecived = "";
        Boolean endChat = false;

        if (startOperation) {
            sendMessageToClientInCaracters.println("INICIALITZAR_CHAT");
            sendMessageToClientInCaracters.flush();
            try {
                do {
                    messageRecived = readClientMessageInCaracters.readLine();
                    System.out.println("SERVER: missatge rebut del client: " + messageRecived);
                    if (messageRecived.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        endChat = true;
                    } else {
                        System.out.println("SERVER: quin missatge vols enviar al client?: ");
                        messageToSend = getScanner().nextLine();

                        if (messageToSend.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            endChat = true;
                        }
                        sendMessageToClientInCaracters.println(messageToSend);
                        sendMessageToClientInCaracters.flush();
                    }
                } while (!endChat);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!startOperation) {
            retornControl(false, sendMessageToClientInCaracters);

            try {
                do {
                    messageRecived = readClientMessageInCaracters.readLine();
                    System.out.println("SERVER: missatge rebut del client: " + messageRecived);

                    if (messageRecived.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        retornControl(false, sendMessageToClientInCaracters);
                        endChat = true;
                    } else {
                        System.out.println("SERVER: quin missatge vols enviar al client?: ");
                        messageToSend = getScanner().nextLine();

                        if (messageToSend.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            endChat = true;
                        }
                        sendMessageToClientInCaracters.println(messageToSend);
                        sendMessageToClientInCaracters.flush();
                    }
                } while (!endChat);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void retornControl(boolean b, PrintWriter sendMessageToClientInCaracters) {
        sendMessageToClientInCaracters.println("RETORN_CONTROL");
        sendMessageToClientInCaracters.flush();
    }

    private static void respondreAPermisConnexio(PrintWriter sendMessageToClientInCaracters) {
        String answer;
        System.out.println("Permets la connexió del client? (S/N) ");
        answer = getScanner().next();

        if (answer.equalsIgnoreCase("S")) {
            sendMessageToClientInCaracters.println("PERMIS_CONNEXIO_CONCEDIT");
        } else {
            sendMessageToClientInCaracters.println("PERMIS_CONNEXIO_DENEGAT");
        }
        sendMessageToClientInCaracters.flush();
    }

    public static String showMenu() {
        StringBuilder menu = new StringBuilder();
        menu.append(System.lineSeparator());
        menu.append("SPU-9 SERVER");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("0. Desconnectar-se del CLIENT");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("1. Retornar el control de les comunicacions al CLIENT");
        menu.append(System.lineSeparator());
        menu.append("2. CHAT");
        menu.append(System.lineSeparator());
        menu.append("3. FTP");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("50. Tancar el sistema");
        menu.append(System.getProperty("line.separator"));
        return menu.toString();
    }

    public static Scanner getScanner() {
        Scanner scanner = new Scanner(System.in);
        return scanner;
    }
}
