package com.shah.exerciciSockets;

import java.io.*;
import java.net.Socket;

import static com.shah.exerciciSockets.SPU_9_server.getScanner;

/**
 * @autor shah
 */
public class SPU_9_cliente {
    private static final String ADDRESS = "127.0.0.1";
    private static final int PORT = 6000;
    private static boolean showMenu = true;

    public static void main(String[] args) throws IOException {
        System.out.println("Cliente Connectado.");
        connectarAmbElServer(ADDRESS, PORT);
        System.out.println("El cliente se ha desconectado.");
    }

    private static void connectarAmbElServer(String address, int port) throws IOException {
        Socket clientSocket;
        BufferedReader inputForSocket_in_caracters;
        PrintWriter outPutToSocket_in_caracters;
        String serverMessage = "";
        boolean endComunications = false;

        clientSocket = new Socket("localhost", 6000);
        inputForSocket_in_caracters = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        outPutToSocket_in_caracters = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);

        System.out.println("Client: demanem permís al server per a connectar-nos.");
        outPutToSocket_in_caracters.println("DEMANAR_PERMIS_CONNEXIO");    //Assegurem que acaba amb un final de línia.
        outPutToSocket_in_caracters.flush();

        serverMessage = inputForSocket_in_caracters.readLine();

        // Rebem si tenim o no tenim permís per a connectar-nos. El permís l'envia el server.
        if (serverMessage.equals("PERMIS_CONNEXIO_CONCEDIT")) {
            System.out.println("Client: tenim permís del server per a connectar-nos.");

            do {
                endComunications = comunicarseAmbElServer(clientSocket, inputForSocket_in_caracters, outPutToSocket_in_caracters);
            } while (endComunications == false);

        } else {
            System.out.println("Client: NO tenim permís del server per a connectar-nos. Tanquem el socket.");
        }

        tancarSocket(clientSocket);
    }

    private static boolean comunicarseAmbElServer(Socket clientSocket, BufferedReader inputForSocket_in_caracters, PrintWriter outPutToSocket_in_caracters) throws IOException {
        int option;
        boolean endConnectionWithClient = false;
        String messageOption = "";
        String serverMessage = "";


        if (showMenu == false) {
            serverMessage = inputForSocket_in_caracters.readLine();

            if (serverMessage != null) {
                System.out.println("CLIENT.procesarMissatgeDelClient(): rebut del server el missatge: '" + serverMessage + "'.");
                messageOption = serverMessage;

                if (messageOption.equals("RETORN_CONTROL")) {
                    showMenu = true;
                }
            } else {
                messageOption = "El mensaje esta nulo";
                showMenu = false;
                endConnectionWithClient = true;
            }
        }

        if (showMenu) {
            do {
                System.out.println(showMenu());
                option = getScanner().nextInt();
                switch (option) {
                    case 0:
                    case 50:
                        endConnectionWithClient = false;
                        break;
                    case 1:
                        retornControl(true, outPutToSocket_in_caracters);
                        showMenu = false;
                        break;
                    case 2:
                        chat(true, inputForSocket_in_caracters, outPutToSocket_in_caracters);
                        System.out.println();
                        break;
                    case 3:
                        ftp(true, clientSocket, inputForSocket_in_caracters, outPutToSocket_in_caracters);
                        break;
                    default:
                        System.out.println("COMANDA NO RECONEGUDA");
                }
            } while ((showMenu) && (option != 50) && (!endConnectionWithClient));
        }
        return endConnectionWithClient;
    }

    private static void retornControl(boolean b, PrintWriter outPutToSocket_in_caracters) {
        outPutToSocket_in_caracters.println("RETORN_CONTROL");
        outPutToSocket_in_caracters.flush();
    }

    private static void ftp(boolean startOperation, Socket clientSocket, BufferedReader readClientMessageInCaracters, PrintWriter sendMessageToClientInCaracters) {

        String messageRecived = "";

        String fileToSend = "serverFile.txt";
        String fileToRecived;

        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;

        OutputStream sendToSocket_in_flux = null;
        InputStream readFromSocket_in_flux = null;

        if (startOperation) {
            try {
                sendMessageToClientInCaracters.println("INICIALITZAR_FTP");
                sendMessageToClientInCaracters.flush();

                messageRecived = readClientMessageInCaracters.readLine();
                System.out.println("Client.ftp: missatge rebut del server " + messageRecived);

                //Enviar el nombre del archivo
                sendMessageToClientInCaracters.println(fileToSend);
                sendMessageToClientInCaracters.flush();

                //Enviar el tamaño del archivo
                File file = new File("dirClient/" + "clientFile.txt");
                sendMessageToClientInCaracters.println(file.length());
                sendMessageToClientInCaracters.flush();

                //Enviar el contenido del archivo
                byte[] bytesArray = new byte[1024 * 16];

                //Sleep for half second
                Thread.sleep(500);

                bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                sendToSocket_in_flux = clientSocket.getOutputStream();

                System.out.println("FTP transmetent... " + fileToSend + "(" + file.length() + " bytes)");
                int readLength = -1;
                int i = 1;
                int numBytesSend = 0;

                while ((readLength = bufferedInputStream.read(bytesArray)) > 0) {
                    sendToSocket_in_flux.write(bytesArray, 0, readLength);
                    sendToSocket_in_flux.flush();

                    numBytesSend = numBytesSend + readLength;
                    System.out.println(i + ": Enviat " + readLength + " bytes del fitxer.");
                    i++;
                }
                System.out.println("Fi d'enviatment de fitxer. Enviats " + numBytesSend + " bytes.");

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                if (bufferedInputStream != null) {
                    try {
                        bufferedInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                messageRecived = readClientMessageInCaracters.readLine();
                System.out.println("Client.fpt(): missatge rebut del server: " + messageRecived);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {

            try {
                // Retornem al client perquè pugui començar el ftp.
                retornControl(false, sendMessageToClientInCaracters);

                // Rebem el nom del fitxer:
                fileToRecived = readClientMessageInCaracters.readLine();
                fileToRecived = "dirClient/" + fileToRecived;

                int tamanyFitxer = Integer.parseInt(readClientMessageInCaracters.readLine());

                System.out.println("Nom del fitxer a rebre: " + fileToRecived);
                System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");

                System.out.println("FTP server rebent...");

                // Rebem el fitxer (podríem rebre diversos blocs de tamany 1024*16 bytes):
                byte[] mybytearray = new byte[1024 * 16];
                readFromSocket_in_flux = clientSocket.getInputStream();
                bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(fileToRecived));
                int count;
                int i = 1;
                int numBytesRebuts = 0;

                while ((count = readFromSocket_in_flux.read(mybytearray)) != -1) { //&& (mybytearray[count-1] != '\0')) {
                    bufferedOutputStream.write(mybytearray, 0, count);

                    numBytesRebuts = numBytesRebuts + count;

                    System.out.println(i + ": Rebut " + count + " bytes del fitxer, mybytearray.length = " + mybytearray.length + ", numBytesRebuts = " + numBytesRebuts);
                    i++;

                    if (numBytesRebuts == tamanyFitxer) {
                        break;
                    }
                }

                bufferedOutputStream.close();

                System.out.println("Nom del fitxer a rebre: " + fileToRecived);
                System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");
                System.out.println("Fi de recepció de fitxer. Rebuts " + numBytesRebuts + " bytes.");

                sendMessageToClientInCaracters.println("FINALITZAT_FTP. Rebut " + numBytesRebuts + " bytes.");
                sendMessageToClientInCaracters.flush();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void chat(boolean startOperation, BufferedReader inputForSocket_in_caracters, PrintWriter outPutToSocket_in_caracters) {
        String messageToSend = "";
        String messageRecived = "";
        Boolean endChat = false;

        if (startOperation) {
            outPutToSocket_in_caracters.println("INICIALITZAR_CHAT");
            outPutToSocket_in_caracters.flush();
            try {
                do {
                    messageRecived = inputForSocket_in_caracters.readLine();
                    System.out.println("Client: missatge rebut del server: " + messageRecived);
                    if (messageRecived.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        endChat = true;
                    } else {
                        System.out.println("Client: quin missatge vols enviar al server?: ");
                        messageToSend = getScanner().nextLine();

                        if (messageToSend.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            endChat = true;
                        }
                        outPutToSocket_in_caracters.println(messageToSend);
                        outPutToSocket_in_caracters.flush();
                    }
                } while (!endChat);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!startOperation) {
            retornControl(false, outPutToSocket_in_caracters);

            try {
                do {
                    messageRecived = inputForSocket_in_caracters.readLine();
                    System.out.println("Client: missatge rebut del server: " + messageRecived);

                    if (messageRecived.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        retornControl(false, outPutToSocket_in_caracters);
                        endChat = true;
                    } else {
                        System.out.println("Client: quin missatge vols enviar al server?: ");
                        messageToSend = getScanner().nextLine();

                        if (messageToSend.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            endChat = true;
                        }
                        outPutToSocket_in_caracters.println(messageToSend);
                        outPutToSocket_in_caracters.flush();
                    }
                } while (!endChat);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void tancarSocket(Socket clientSocket) {
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static String showMenu() {
        StringBuilder menu = new StringBuilder();
        menu.append(System.lineSeparator());
        menu.append("SPU-9 CLIENT");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("0. Desconnectar-se del SERVER");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("1. Retornar el control de les comunicacions al SERVER");
        menu.append(System.lineSeparator());
        menu.append("2. CHAT");
        menu.append(System.lineSeparator());
        menu.append("3. FTP");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("50. Tancar el sistema");
        menu.append(System.getProperty("line.separator"));
        return menu.toString();
    }

}
