package com.shah.exerciciSockets;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * @autor shah
 */
public class SPU_9_Server2 {

    static final int PORT = 6000;
    private static boolean endComunication = false;

    /*
    Els identificadors del tipus de transmissió que es farà són:
        DEMANAR_PERMIS_CONNEXIO: La envía el cliente al server para Pedirle Permiso de conexión.
        PERMIS_CONNEXIO_CONCEDIT: La envía el server al cliente para indicarle que tiene permiso para aconnectarse.
        RETORN_CONTROL: Es para pasarle el control de las comunicaciones al otro.
        INICIALITZAR_CHAT: Es para indicar que queremos chatear.
        FINALITZAR_CHAT: Es para indicar que queremos terminar de chatear.
        INICIALITZAR_FTP: Es para indicar que queremos enviar un archivo a través de FTP.
     */

    public static void main(String[] args) throws IOException {
        System.out.println("El servidor se ha iniciado.");
        escoltar();
        System.out.println("El servidor ha sido cerrado!");
    }


    private static void escoltar() throws IOException {

        ServerSocket serverSocket = new ServerSocket(PORT);
        Socket clientSocket = null;

        while (!endComunication) {

            //Servidor esuchando para que algun cliente se connecte
            //El cliente es quien inicializa la comunicación.
            clientSocket = serverSocket.accept();
            System.out.println("Cliente conectado");


            //Procesamos la petición del cliente.
            procesarComunicacionsAmbClient(clientSocket);
            //PARECE QUE ESTE SERVER SÓLO PUEDE TRABAJAR CON 1 CLIENTE A LA HORA.
            //Cuando se desconecte el cliente, esperará a que se conecte otro y por lo tanto el server no se detendrá nunca.


            //Cierra el socket abierto del cliente
            tancarClient(clientSocket);
            System.out.println("Cliente desconectado. Servidor esperando que otro cliente se conecte...");
        }

        //Cerrar el socket de servidor
        if ((serverSocket != null) && (!serverSocket.isClosed())) {
            serverSocket.close();
        }
    }


    private static void procesarComunicacionsAmbClient(Socket clientSocket) {
        boolean endComunication = false;
        try {
            //Abrimos entrada y salida para caracteres en el socket.
            BufferedReader readClientMessageInCaracters = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter sendMessageToClientInCaracters = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            //Este proceso se repite hasta que no termine la comunicación.
            do {
                String clientMessage = readClientMessageInCaracters.readLine();
                endComunication = procesarMissatgesDelClient(clientSocket, clientMessage, readClientMessageInCaracters, sendMessageToClientInCaracters);
            } while (!endComunication);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static boolean procesarMissatgesDelClient(Socket clientSocket, String clientMessage, BufferedReader readClientMessageInCaracters, PrintWriter sendMessageToClientInCaracters) {

        boolean endConnectionWithClient = false;
        String messageType = "";
        int selectedOption;
        Boolean showMenu = false;


        if (clientMessage == null) {
            clientMessage = "El mensaje recibido es nulo";
            endConnectionWithClient = true;
        } else {
            messageType = clientMessage;
            switch (messageType) {
                case "DEMANAR_PERMIS_CONNEXIO":
                    respondreAPermisConnexio(sendMessageToClientInCaracters);
                    break;
                case "RETORN_CONTROL":
                    showMenu = true;
                case "INICIALITZAR_CHAT":
                    chat(false, readClientMessageInCaracters, sendMessageToClientInCaracters);
                    System.out.println();
                    break;
                case "INICIALITZAR_FTP":
                    ftp(false, clientSocket, readClientMessageInCaracters, sendMessageToClientInCaracters);
                    System.out.println();
                    break;
            }
        }


        if (showMenu) {
            do {
                //Mostrar menu
                System.out.println(showMenu());
                selectedOption = getScanner().nextInt();

                switch (selectedOption) {
                    case 0:
                    case 50:
                        endConnectionWithClient = false;
                        showMenu = false;
                        break;
                    case 1:
                        retornControl(true, sendMessageToClientInCaracters);
                        showMenu = false;
                        break;
                    case 2:
                        chat(true, readClientMessageInCaracters, sendMessageToClientInCaracters);
                        System.out.println();
                        break;
                    case 3:
                        ftp(true, clientSocket, readClientMessageInCaracters, sendMessageToClientInCaracters);
                        break;
                    default:
                        System.out.println("COMANDA NO RECONEGUDA");
                }
            } while ((showMenu) && (selectedOption != 50 || selectedOption != 0) && (!endConnectionWithClient));
        }

        return endConnectionWithClient;
    }


    private static void retornControl(boolean b, PrintWriter sendMessageToClientInCaracters) {
        sendMessageToClientInCaracters.println("RETORN_CONTROL");
        sendMessageToClientInCaracters.flush();
    }


    private static void respondreAPermisConnexio(PrintWriter sendMessageToClientInCaracters) {
        String answer;
        System.out.println("Permets la connexió del client? (S/N) ");
        answer = getScanner().next();

        if (answer.equalsIgnoreCase("S")) {
            sendMessageToClientInCaracters.println("PERMIS_CONNEXIO_CONCEDIT");
        } else {
            sendMessageToClientInCaracters.println("PERMIS_CONNEXIO_DENEGAT");
        }
        sendMessageToClientInCaracters.flush();
    }


    private static void tancarClient(Socket clientSocket) {
        try {
            if (clientSocket != null && !clientSocket.isClosed()) {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            //Logger.getLogger(TcpSocketServer.class.getName().log(Level.SEVERE, null, e));
        }
    }


    private static void chat(boolean startOperation, BufferedReader readClientMessageInCaracters, PrintWriter sendMessageToClientInCaracters) {
        String messageToSend = "";
        String messageReceived = "";
        Boolean endChat = false;

        //El servidor inicializa el chat.
        if (startOperation) {
            sendMessageToClientInCaracters.println("INICIALITZAR_CHAT");
            sendMessageToClientInCaracters.flush();
            try {
                do {
                    messageReceived = readClientMessageInCaracters.readLine();
                    System.out.println("SERVER: missatge rebut del client: " + messageReceived);
                    if (messageReceived.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        endChat = true;
                    } else {
                        System.out.println("SERVER: quin missatge vols enviar al client?: ");
                        messageToSend = getScanner().nextLine();

                        if (messageToSend.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            endChat = true;
                        }
                        sendMessageToClientInCaracters.println(messageToSend);
                        sendMessageToClientInCaracters.flush();
                    }
                } while (!endChat);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //El servidor no inicializa el chat.
        if (!startOperation) {
            retornControl(false, sendMessageToClientInCaracters);
            try {
                do {
                    messageReceived = readClientMessageInCaracters.readLine();
                    System.out.println("SERVER: missatge rebut del client: " + messageReceived);

                    if (messageReceived.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        // Retornem al client el control de les comunicacions.
                        retornControl(false, sendMessageToClientInCaracters);
                        endChat = true;
                    } else {
                        System.out.println("SERVER: quin missatge vols enviar al client?: ");
                        messageToSend = getScanner().nextLine();

                        if (messageToSend.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            endChat = true;
                        }
                        sendMessageToClientInCaracters.println(messageToSend);
                        sendMessageToClientInCaracters.flush();
                    }
                } while (!endChat);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void ftp(boolean startOperation, Socket clientSocket, BufferedReader readClientMessageInCaracters, PrintWriter sendMessageToClientInCaracters) {

        String messageRecived = "";

        String fileToSend = "serverFile.txt";
        String fileToRecived;

        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;

        InputStream readFromSocket_in_flux;
        OutputStream sendToSocket_in_flux;


        if (startOperation) {
            try {
                sendMessageToClientInCaracters.println("INICIALITZAR_FTP");
                sendMessageToClientInCaracters.flush();

                messageRecived = readClientMessageInCaracters.readLine();
                System.out.println("ServerFtp: missatge rebut del client " + messageRecived);

                //Enviamos el nombre del fichero
                sendMessageToClientInCaracters.println(fileToSend);
                sendMessageToClientInCaracters.flush();

                //Enviamos el tamaño del fichero
                File file = new File("dirServidor/" + "serverFile.txt");
                sendMessageToClientInCaracters.println(file.length());
                sendMessageToClientInCaracters.flush();

                //Enviamos el contenido del fichero
                byte[] bytesArray = new byte[1024 * 16];

                //Sleep for half second
                Thread.sleep(500);

                //leemos el fichero que queremos enviar en bloques de tamaño 1024*16
                bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                sendToSocket_in_flux = clientSocket.getOutputStream();

                System.out.println("FTP transmetent... " + fileToSend + "(" + file.length() + " bytes)");
                int readLength = -1;
                int i = 1;
                int numBytesSend = 0;

                while ((readLength = bufferedInputStream.read(bytesArray)) > 0) {
                    sendToSocket_in_flux.write(bytesArray, 0, readLength);
                    sendToSocket_in_flux.flush();

                    numBytesSend = numBytesSend + readLength;
                    System.out.println(i + ": Enviat " + readLength + " bytes del fitxer.");
                    i++;
                }
                System.out.println("Fi d'enviatment de fitxer. Enviats " + numBytesSend + " bytes.");

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                if (bufferedInputStream != null) {
                    try {
                        bufferedInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                messageRecived = readClientMessageInCaracters.readLine();
                System.out.println("SERVER.fpt(): missatge rebut del client: " + messageRecived);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {

            try {
                // Retornem al client perquè pugui començar el ftp.
                retornControl(false, sendMessageToClientInCaracters);

                // Rebem el nom del fitxer:
                fileToRecived = readClientMessageInCaracters.readLine();
                fileToRecived = "dirServidor/" + fileToRecived;

                int tamanyFitxer = Integer.parseInt(readClientMessageInCaracters.readLine());

                System.out.println("Nom del fitxer a rebre: " + fileToRecived);
                System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");

                System.out.println("FTP server rebent...");

                // Rebem el fitxer (podríem rebre diversos blocs de tamany 1024*16 bytes):
                byte[] mybytearray = new byte[1024 * 16];
                readFromSocket_in_flux = clientSocket.getInputStream();
                bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(fileToRecived));
                int count;
                int i = 1;
                int numBytesRebuts = 0;

                while ((count = readFromSocket_in_flux.read(mybytearray)) != -1) { //&& (mybytearray[count-1] != '\0')) {

                    bufferedOutputStream.write(mybytearray, 0, count);
                    numBytesRebuts = numBytesRebuts + count;

                    System.out.println(i + ": Rebut " + count + " bytes del fitxer, mybytearray.length = " + mybytearray.length + ", numBytesRebuts = " + numBytesRebuts);
                    i++;

                    if (numBytesRebuts == tamanyFitxer) {
                        break;
                    }
                }

                bufferedOutputStream.close();

                System.out.println("Nom del fitxer a rebre: " + fileToRecived);
                System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");
                System.out.println("Fi de recepció de fitxer. Rebuts " + numBytesRebuts + " bytes.");

                sendMessageToClientInCaracters.println("FINALITZAT_FTP. Rebut " + numBytesRebuts + " bytes.");
                sendMessageToClientInCaracters.flush();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static String showMenu() {
        StringBuilder menu = new StringBuilder();
        menu.append(System.lineSeparator());
        menu.append("SPU-9 SERVER");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("0. Desconnectar-se del CLIENT");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("1. Retornar el control de les comunicacions al CLIENT");
        menu.append(System.lineSeparator());
        menu.append("2. CHAT");
        menu.append(System.lineSeparator());
        menu.append("3. FTP");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("50. Tancar el sistema");
        menu.append(System.getProperty("line.separator"));
        return menu.toString();
    }

    public static Scanner getScanner() {
        Scanner scanner = new Scanner(System.in);
        return scanner;
    }

}
