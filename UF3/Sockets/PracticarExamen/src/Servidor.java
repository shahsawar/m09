import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * @autor shah
 */
public class Servidor {

    static final int PORT = 6000;

    public static void main(String[] args) {

        //Variables
        ServerSocket serverSocket = null;
        Socket clientSocket;
        boolean endComunication = false;

        try {
            //Inicializar el servidor
            System.out.println("El servidor se ha iniciado.");
            serverSocket = new ServerSocket(PORT);

            while (!endComunication) {

                //Esperando que algún cliente se conecte
                clientSocket = serverSocket.accept();
                System.out.println("Cliente conectado al servidor.");

                //Iniciar el chat y cierre el cliente después de terminar el chat
                chat(clientSocket);
                closeClient(clientSocket);

                System.out.println("Esperando que otro cliente se connecte...");
            }

            //Cerrar el socket principal
            if ((serverSocket != null) && (!serverSocket.isClosed())) {
                serverSocket.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void chat(Socket clientSocket) throws IOException {
        boolean endChat = false;
        String messageToSend = "";
        String messageReceived = "";
        BufferedReader input;
        PrintWriter output;

        input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        output = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);

        while (!endChat) {

            //Mensaje recibido
            messageReceived = input.readLine();

            if (messageReceived != null && (!messageReceived.equals("exit"))) {
                System.out.println("SERVER: message received from the client: " + messageReceived);

                //Mensaje a enviar
                System.out.print("SERVER: what message do you want to send to the client?: ");
                messageToSend = getScanner().nextLine();
                output.println(messageToSend);
                output.flush();

                if (messageReceived.equals("exit")) {
                    endChat = true;
                }
            } else {
                endChat = true;
            }
        }
        closeClient(clientSocket);
    }

    private static void closeClient(Socket clientSocket) {
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static Scanner getScanner() {
        Scanner sc = new Scanner(System.in);
        return sc;
    }

}


