import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * @autor shah
 */
public class Cliente {

    private static final String ADDRESS = "127.0.0.1";
    private static final int PORT = 6000;

    public static void main(String[] args) {

        //Variables
        Socket clientSocket;
        BufferedReader input;
        PrintWriter output;
        String messageToSend = "";
        String messageReceived = "";
        boolean endChat = false;

        try {
            //Conectar el cliente al servidor e inicializar el BufferReader y PrintWriter
            clientSocket = new Socket(InetAddress.getByName(ADDRESS), PORT);
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            output = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);

            //Mostrar el menú del cliente y guardar la opción seleccionada.
            System.out.print(showMenu() + ":");
            int option = getScanner().nextInt();

            switch (option) {
                case 0:
                case 50:
                    endChat = true;
                    break;
                case 1:
                    while (!endChat) {

                        //Inicializando la comunicación con el server.
                        System.out.print("CLIENT: What message do you want to send to the client?: ");
                        messageToSend = getScanner().next();
                        output.println(messageToSend);
                        output.flush();

                        //Leer el mensaje del cliente
                        messageReceived = input.readLine();
                        System.out.println("CLIENT: message received from the servidor: " + messageReceived);

                        //Condición de salida
                        if ((messageReceived != null) && (messageReceived.equals("exit")) || messageToSend.equals("exit")) {
                            endChat = true;
                        }
                    }
                    //Cerrar el cliente
                    closeClient(clientSocket);
                    break;
                default:
                    System.out.println("The selected option is invalid!");
            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void closeClient(Socket clientSocket) {
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static Scanner getScanner() {
        Scanner sc = new Scanner(System.in);
        return sc;
    }

    public static String showMenu() {
        StringBuilder menu = new StringBuilder();
        menu.append(System.lineSeparator());
        menu.append("SPU-9 CLIENT");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("0. Desconnectar-se del SERVIDOR");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("1. Chat");
        menu.append(System.lineSeparator());
        menu.append("2. ");
        menu.append(System.lineSeparator());
        menu.append("3. ");
        menu.append(System.lineSeparator());
        menu.append(System.lineSeparator());
        menu.append("50. Tancar el sistema");
        menu.append(System.getProperty("line.separator"));
        return menu.toString();
    }
}
