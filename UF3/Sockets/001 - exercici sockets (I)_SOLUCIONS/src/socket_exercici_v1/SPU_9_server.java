package socket_exercici_v1;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;


import Pantalles.MenuConstructorPantalla;

public class SPU_9_server {
    private static final int PORT = 9090;
    private static final String separador = "$##$";
    private static boolean fiComunicacio = false;
    private static boolean veureMenu = true; // La fem static perquè sigui accesssible des de qualsevol mètode de la classe.


    public static void main(String[] args) {
        System.out.println("SPU-9 Server - INICI");
        escoltar();
        System.out.println("SPU-9 Server - FI");
    }

    private static void escoltar() {
        ServerSocket serverSocket = null;
        Socket clientSocket = null;

        try {
            // Es crea un ServerSocket que atendrà el port nº PORT a l'espera de clients que demanin comunicar-se.
            serverSocket = new ServerSocket(PORT);

            while (!fiComunicacio) {
                // El mètode accept() resta a l'espera d'una petició i en el moment de produir-se crea una instància
                // específica de sòcol per suportar la comunicació amb el client acceptat.
                // El client és qui inicialitza la comunicació.
                clientSocket = serverSocket.accept();

                // Processem la petició del client.
                procesarComunicacionsAmbClient(clientSocket);
                // SEMBLA QUE AQUEST SERVER NOMÉS POT TREBALLAR AMB 1 CLIENT A L'HORA.
                // Quan es desconnecti el client, esperarà a que es connecti un altre i per tant el server no s'aturarà mai.

                // Tanquem el sòcol temporal que vam obrir per atendre al client.
                tancarClient(clientSocket);

                System.out.println("SERVER.escoltar(): el client s'ha desconnectat, esperant a que es connecti un altre client...");
            }

            // Tanquem el sòcol principal
            if ((serverSocket != null) && (!serverSocket.isClosed())) {
                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void procesarComunicacionsAmbClient(Socket clientSocket) {
        String missatgeDelClient = "";
        BufferedReader entradaPelClientSocket_en_caracters = null;
        PrintWriter sortidaCapAlClientSocket_en_caracters = null;
        String dadesAEnviarAlClient[] = {"", ""};
        boolean acabarComunicacio = false;


        try {
            // Obrim entrada i sortida per caràcters en el socket.
            entradaPelClientSocket_en_caracters = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            sortidaCapAlClientSocket_en_caracters = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);

            // Processa la petició de connexió del client. El client és qui inicialitza la connexió.
            do {
                missatgeDelClient = entradaPelClientSocket_en_caracters.readLine();

                acabarComunicacio = procesarMissatgesDelClient(clientSocket, missatgeDelClient, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);

            } while (acabarComunicacio == false);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static void tancarClient(Socket clientSocket) {
        //Si falla el tancament no podem fer gaire cosa, només enregistrar el problema.

        //Tancament de tots els recursos.
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static boolean procesarMissatgesDelClient(Socket clientSocket, String missatgeDelClient, BufferedReader entradaPelClientSocket_en_caracters, PrintWriter sortidaCapAlClientSocket_en_caracters) {
        String opcio;
        Scanner sc = new Scanner(System.in);
        StringBuilder menu = new StringBuilder("");
        String tipusMissatge = "";
        StringTokenizer st;
        boolean acabarConnexioAmbElClient = false;
        //boolean veureMenu = false;


        // El client és qui inicialitza la comunicació i per tant enviarà un 'DEMANAR_PERMIS_CONNEXIO'.

        // Aquest codi és per quan el client talla la comunicació, al cridar tancarSocket() s'envia
        // automàticament un NULL pel socket que ens arriba aquí.
        // SEMBLA QUE QUAN S'ACABA D'ENVIAR UN FITXER AMB EL FTP TAMBÉ PASSA AIXÒ
        if (missatgeDelClient != null) {
            tipusMissatge = missatgeDelClient;
        } else {
            tipusMissatge = "patata";
            acabarConnexioAmbElClient = true;
        }

        veureMenu = false;

        if (tipusMissatge.equals("DEMANAR_PERMIS_CONNEXIO")) {
            respondreAPermisConnexio(sortidaCapAlClientSocket_en_caracters);
        }

        if (tipusMissatge.equals("RETORN_CONTROL")) {
            veureMenu = true;
        }

        if (tipusMissatge.equals("INICIALITZAR_CHAT")) {
            chat(false, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);
            System.out.println();
        }

        if (tipusMissatge.equals("INICIALITZAR_FTP")) {
            try {
                ftp(false, clientSocket, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println();
        }


        if (veureMenu == true) {
            do {
                menu.delete(0, menu.length());

                menu.append(System.getProperty("line.separator"));
                menu.append("SPU-9 SERVER ");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));

                menu.append("0. Desconnectar-se del CLIENT");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));
                menu.append("1. Retornar el control de les comunicacions al CLIENT");
                menu.append(System.getProperty("line.separator"));
                menu.append("2. CHAT");
                menu.append(System.getProperty("line.separator"));
                menu.append("3. FTP");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));

                menu.append("50. Tancar el sistema");
                menu.append(System.getProperty("line.separator"));

                System.out.print(MenuConstructorPantalla.constructorPantalla(menu));

                opcio = sc.next();

                switch (opcio) {
                    case "0":
                        acabarConnexioAmbElClient = true;
                        veureMenu = false;
                        break;
                    case "1":
                        retornControl(true, sortidaCapAlClientSocket_en_caracters);
                        veureMenu = false;
                        break;
                    case "2":
                        chat(true, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);
                        System.out.println();
                        break;
                    case "3":
                        try {
                            ftp(true, clientSocket, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println();
                        break;
                    case "50":
                        acabarConnexioAmbElClient = true;
                        veureMenu = false;
                        break;
                    default:
                        System.out.println("COMANDA NO RECONEGUDA");
                }
            } while ((!opcio.equals("50")) && (veureMenu == true) && (acabarConnexioAmbElClient == false));
        }

        return acabarConnexioAmbElClient;
    }

    private static void respondreAPermisConnexio(PrintWriter sortidaCapAlClientSocket_en_caracters) {
        Scanner sc = new Scanner(System.in);
        String resposta;

        System.out.print("Permets la connexió del client? (S/N) ");
        resposta = sc.next();

        if (resposta.equalsIgnoreCase("S")) {
            sortidaCapAlClientSocket_en_caracters.println("PERMIS_CONNEXIO_CONCEDIT");
        } else {
            sortidaCapAlClientSocket_en_caracters.println("PERMIS_CONNEXIO_DENEGAT");
        }
        sortidaCapAlClientSocket_en_caracters.flush();
    }


    private static void chat(boolean inicialitzemOperacio, BufferedReader entradaPelClientSocket_en_caracters, PrintWriter sortidaCapAlClientSocket_en_caracters) {
        String missatgeAEnviar = "";
        String missatgeRebut = "";
        Scanner sc2 = new Scanner(System.in);
        boolean acabarChat = false;


        // El server inicialitza el chat.
        if (inicialitzemOperacio) {
            sortidaCapAlClientSocket_en_caracters.println("INICIALITZAR_CHAT");
            sortidaCapAlClientSocket_en_caracters.flush();

            try {
                do {
                    missatgeRebut = entradaPelClientSocket_en_caracters.readLine();
                    System.out.println("SERVER.chatejar(): missatge rebut del client: " + missatgeRebut);

                    if (missatgeRebut.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        acabarChat = true;
                    } else {
                        System.out.print("SERVER.chatejar(): quin missatge vols enviar al client?: ");
                        missatgeAEnviar = sc2.nextLine();

                        if (missatgeAEnviar.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            acabarChat = true;
                        }

                        sortidaCapAlClientSocket_en_caracters.println(missatgeAEnviar);
                        sortidaCapAlClientSocket_en_caracters.flush();
                    }

                } while (acabarChat == false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // El server NO inicialitza el chat.
        if (!inicialitzemOperacio) {
            retornControl(false, sortidaCapAlClientSocket_en_caracters);        // Retornem al client perquè pugui començar el chat.

            try {
                do {
                    missatgeRebut = entradaPelClientSocket_en_caracters.readLine();
                    System.out.println("SERVER.chatejar(): missatge rebut del client: " + missatgeRebut);

                    if (missatgeRebut.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        retornControl(false, sortidaCapAlClientSocket_en_caracters);        // Retornem al client el control de les comunicacions.

                        acabarChat = true;
                    } else {
                        System.out.print("SERVER.chatejar(): quin missatge vols enviar al client?: ");
                        missatgeAEnviar = sc2.nextLine();

                        if (missatgeAEnviar.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            acabarChat = true;
                        }

                        sortidaCapAlClientSocket_en_caracters.println(missatgeAEnviar);
                        sortidaCapAlClientSocket_en_caracters.flush();
                    }
                } while (acabarChat == false);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private static void retornControl(boolean inicialitzemOperacio, PrintWriter sortidaCapAlSocket_en_caracters) {
        sortidaCapAlSocket_en_caracters.println("RETORN_CONTROL");
        sortidaCapAlSocket_en_caracters.flush();
    }


    private static void ftp(boolean inicialitzemOperacio, Socket socket, BufferedReader entradaPelSocket_en_caracters, PrintWriter sortidaCapAlSocket_en_caracters) throws IOException {
        String FILE_TO_SEND = "battlestar galactica.bmp";
        BufferedInputStream bis = null;
        OutputStream sortidaCapAlSocket_en_flux = null;
        String missatgeRebut = "";

        String FILE_TO_RECEIVED;
        InputStream entradaPelSocket_en_flux;
        BufferedOutputStream bos = null;


        // Si inicialitzemoperacio és que el client vol enviar un fitxer al server.
        if (inicialitzemOperacio) {
            sortidaCapAlSocket_en_caracters.println("INICIALITZAR_FTP");
            sortidaCapAlSocket_en_caracters.flush();

            // Si no fessim les següents 2 línies, que no serveixen per a res, no faria falta que el server fes un retornControl().
            missatgeRebut = entradaPelSocket_en_caracters.readLine();
            System.out.println("SERVER.ftp(): missatge rebut del client: " + missatgeRebut);

            // Enviem el nom del fitxer:
            sortidaCapAlSocket_en_caracters.println(FILE_TO_SEND);
            sortidaCapAlSocket_en_caracters.flush();

            // Enviem el tamany del fitxer:
            File myFile = new File("dirServidor/" + FILE_TO_SEND);
            sortidaCapAlSocket_en_caracters.println(myFile.length());
            sortidaCapAlSocket_en_caracters.flush();

            // Enviem el contingut del fitxer:
            byte[] mybytearray = new byte[1024 * 16];        // Fem un array on entraran fins a 1024*16 bytes (1 byte = 1 caràcter del fitxer).

            try {
                //LI FEM AQUEST SLEEP PERQUE SINÒ MOLTES VEGADES CASCA L'ARRIBADA DE LA INFO AL SERVER.
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // LLegim el fitxer que volem enviar en blocs de tamany 1024*16 i els anem enviant:
                bis = new BufferedInputStream(new FileInputStream(myFile));
                sortidaCapAlSocket_en_flux = socket.getOutputStream();

                System.out.println("FTP transmetent... " + FILE_TO_SEND + "(" + myFile.length() + " bytes)");
                int readLength = -1;
                int i = 1;
                int numBytesEnviats = 0;

                while ((readLength = bis.read(mybytearray)) > 0) {
                    sortidaCapAlSocket_en_flux.write(mybytearray, 0, readLength);
                    sortidaCapAlSocket_en_flux.flush();

                    numBytesEnviats = numBytesEnviats + readLength;

                    System.out.println(i + ": Enviat " + readLength + " bytes del fitxer.");
                    i++;
                }

                System.out.println("Fi d'enviatment de fitxer. Enviats " + numBytesEnviats + " bytes.");

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bis != null) bis.close();
            }

            missatgeRebut = entradaPelSocket_en_caracters.readLine();
            System.out.println("SERVER.fpt(): missatge rebut del client: " + missatgeRebut);

        } else {
            // Si NO inicialitzemoperacio és que el client envia un fitxer al server.

            retornControl(false, sortidaCapAlSocket_en_caracters);        // Retornem al client perquè pugui començar el ftp.

            // Rebem el nom del fitxer:
            FILE_TO_RECEIVED = entradaPelSocket_en_caracters.readLine();
            FILE_TO_RECEIVED = "dirServidor/" + FILE_TO_RECEIVED;

            int tamanyFitxer = Integer.parseInt(entradaPelSocket_en_caracters.readLine());

            System.out.println("Nom del fitxer a rebre: " + FILE_TO_RECEIVED);
            System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");

            System.out.println("FTP server rebent...");

            // Rebem el fitxer (podríem rebre diversos blocs de tamany 1024*16 bytes):
            byte[] mybytearray = new byte[1024 * 16];
            entradaPelSocket_en_flux = socket.getInputStream();
            bos = new BufferedOutputStream(new FileOutputStream(FILE_TO_RECEIVED));
            int count;
            int i = 1;
            int numBytesRebuts = 0;

            while ((count = entradaPelSocket_en_flux.read(mybytearray)) != -1) { //&& (mybytearray[count-1] != '\0')) {
                bos.write(mybytearray, 0, count);

                numBytesRebuts = numBytesRebuts + count;

                System.out.println(i + ": Rebut " + count + " bytes del fitxer, mybytearray.length = " + mybytearray.length + ", numBytesRebuts = " + numBytesRebuts);
                i++;

                if (numBytesRebuts == tamanyFitxer) {
                    break;
                }
            }

            bos.close();

            System.out.println("Nom del fitxer a rebre: " + FILE_TO_RECEIVED);
            System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");

            System.out.println("Fi de recepció de fitxer. Rebuts " + numBytesRebuts + " bytes.");

            sortidaCapAlSocket_en_caracters.println("FINALITZAT_FTP. Rebut " + numBytesRebuts + " bytes.");
            sortidaCapAlSocket_en_caracters.flush();
        }
    }

}
