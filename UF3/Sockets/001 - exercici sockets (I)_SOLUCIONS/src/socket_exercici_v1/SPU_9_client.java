package socket_exercici_v1;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;

import Pantalles.MenuConstructorPantalla;

public class SPU_9_client {
    private static final String ADDRESS = "127.0.0.1";
    private static final int PORT = 9090;
    private static final String separador = "$##$";
    private static boolean veureMenu = true;                // La fem static perquè sigui accesssible des de qualsevol mètode de la classe.


    public static void main(String[] args) {
        System.out.println("SPU-9 Client - INICI");
        connectarAmbElServer(ADDRESS, PORT);
        System.out.println("SPU-9 Client - FI");
    }

    private static void connectarAmbElServer(String address, int port) {
        Socket socket;
        BufferedReader entradaPelSocket_en_caracters;
        PrintWriter sortidaCapAlSocket_en_caracters = null;
        String missatgeDelServer = "";
        boolean acabarComunicacio = false;


        // El client és qui inicialitza la comunicació.

        try {
            socket = new Socket(InetAddress.getByName(address), port);
            entradaPelSocket_en_caracters = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            sortidaCapAlSocket_en_caracters = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);


            // Enviament de la petició de connexió. Inicialitzant la comunicació amb el server.
            System.out.println("Client: demanem permís al server per a connectar-nos.");
            sortidaCapAlSocket_en_caracters.println("DEMANAR_PERMIS_CONNEXIO");    //Assegurem que acaba amb un final de línia.
            sortidaCapAlSocket_en_caracters.flush();

            missatgeDelServer = entradaPelSocket_en_caracters.readLine();

            // Rebem si tenim o no tenim permís per a connectar-nos. El permís l'envia el server.
            if (missatgeDelServer.equals("PERMIS_CONNEXIO_CONCEDIT")) {
                System.out.println("Client: tenim permís del server per a connectar-nos.");

                do {
                    acabarComunicacio = comunicarseAmbElServer(socket, entradaPelSocket_en_caracters, sortidaCapAlSocket_en_caracters);
                } while (acabarComunicacio == false);

            } else {
                System.out.println("Client: NO tenim permís del server per a connectar-nos. Tanquem el socket.");
            }

            tancarSocket(socket);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean comunicarseAmbElServer(Socket socket, BufferedReader entradaPelSocket_en_caracters, PrintWriter sortidaCapAlSocket_en_caracters) {
        String missatgeDelServer = "";
        String opcio;
        Scanner sc = new Scanner(System.in);
        StringBuilder menu = new StringBuilder("");
        String tipusMissatge = "";
        StringTokenizer st;
        //boolean veureMenu = true;
        boolean acabarConnexioAmbElServer = false;


        if (veureMenu == false) {
            try {
                missatgeDelServer = entradaPelSocket_en_caracters.readLine();

                // Aquest codi és per quan el server talla la comunicació, al cridar tancarSocket() s'envia
                // automàticament un NULL pel socket que ens arriba aquí.
                if (missatgeDelServer != null) {
                    System.out.println("CLIENT.procesarMissatgeDelClient(): rebut del server el missatge: '" + missatgeDelServer + "'.");
                    tipusMissatge = missatgeDelServer;

                    if (tipusMissatge.equals("RETORN_CONTROL")) {
                        veureMenu = true;
                    }
                } else {
                    tipusMissatge = "patata";
                    veureMenu = false;
                    acabarConnexioAmbElServer = true;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if (veureMenu == true) {
            do {
                menu.delete(0, menu.length());

                menu.append(System.getProperty("line.separator"));
                menu.append("SPU-9 CLIENT ");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));

                menu.append("0. Desconnectar-se del SERVER");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));
                menu.append("1. Retornar el control de les comunicacions al SERVER");
                menu.append(System.getProperty("line.separator"));
                menu.append("2. CHAT");
                menu.append(System.getProperty("line.separator"));
                menu.append("3. FTP");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));

                menu.append("50. Tancar el sistema");
                menu.append(System.getProperty("line.separator"));

                System.out.print(MenuConstructorPantalla.constructorPantalla(menu));

                opcio = sc.next();

                switch (opcio) {
                    case "0":
                        acabarConnexioAmbElServer = true;
                        break;
                    case "1":
                        retornControl(true, sortidaCapAlSocket_en_caracters);
                        veureMenu = false;
                        break;
                    case "2":
                        chat(true, entradaPelSocket_en_caracters, sortidaCapAlSocket_en_caracters);
                        System.out.println();
                        break;
                    case "3":
                        try {
                            ftp(true, socket, entradaPelSocket_en_caracters, sortidaCapAlSocket_en_caracters);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println();
                        break;
                    case "50":
                        acabarConnexioAmbElServer = true;
                        break;
                    default:
                        System.out.println("COMANDA NO RECONEGUDA");
                }
            } while ((!opcio.equals("50")) && (veureMenu == true) && (acabarConnexioAmbElServer == false));
        }

        return acabarConnexioAmbElServer;
    }

    private static void tancarSocket(Socket clientSocket) {
        //Si falla el tancament no podem fer gaire cosa, només enregistrar el problema.

        //Tancament de tots els recursos.
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    private static void chat(boolean inicialitzemOperacio, BufferedReader entradaPelSocket_en_caracters, PrintWriter sortidaCapAlSocket_en_caracters) {
        String missatgeAEnviar = "";
        String missatgeRebut = "";
        Scanner sc2 = new Scanner(System.in);
        boolean acabarChat = false;


        // El client inicialitza el chat.
        if (inicialitzemOperacio) {
            sortidaCapAlSocket_en_caracters.println("INICIALITZAR_CHAT");
            sortidaCapAlSocket_en_caracters.flush();

            try {
                do {
                    missatgeRebut = entradaPelSocket_en_caracters.readLine();
                    System.out.println("CLIENT.chatejar(): missatge rebut del server: " + missatgeRebut);

                    if (missatgeRebut.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        acabarChat = true;
                    } else {
                        System.out.print("CLIENT.chatejar(): quin missatge vols enviar al server?: ");
                        missatgeAEnviar = sc2.nextLine();

                        if (missatgeAEnviar.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            acabarChat = true;
                        }

                        sortidaCapAlSocket_en_caracters.println(missatgeAEnviar);
                        sortidaCapAlSocket_en_caracters.flush();
                    }

                } while (acabarChat == false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // El server NO inicialitza el chat.
        if (!inicialitzemOperacio) {
            retornControl(false, sortidaCapAlSocket_en_caracters);        // Retornem al server perquè pugui començar el chat.

            try {
                do {
                    missatgeRebut = entradaPelSocket_en_caracters.readLine();
                    System.out.println("CLIENT.chatejar(): missatge rebut del server: " + missatgeRebut);

                    if (missatgeRebut.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        retornControl(false, sortidaCapAlSocket_en_caracters);        // Retornem al server el control de les comunicacions.

                        acabarChat = true;
                    } else {
                        System.out.print("CLIENT.chatejar(): quin missatge vols enviar al server?: ");
                        missatgeAEnviar = sc2.nextLine();

                        if (missatgeAEnviar.equalsIgnoreCase("FINALITZAR_CHAT")) {
                            acabarChat = true;
                        }

                        sortidaCapAlSocket_en_caracters.println(missatgeAEnviar);
                        sortidaCapAlSocket_en_caracters.flush();
                    }
                } while (acabarChat == false);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private static void retornControl(boolean inicialitzemOperacio, PrintWriter sortidaCapAlSocket_en_caracters) {
        sortidaCapAlSocket_en_caracters.println("RETORN_CONTROL");
        sortidaCapAlSocket_en_caracters.flush();
    }


    private static void ftp(boolean inicialitzemOperacio, Socket socket, BufferedReader entradaPelSocket_en_caracters, PrintWriter sortidaCapAlSocket_en_caracters) throws IOException {
        String FILE_TO_SEND = "hola.txt";
        BufferedInputStream bis = null;
        OutputStream sortidaCapAlSocket_en_flux = null;
        String missatgeRebut = "";

        String FILE_TO_RECEIVED;
        InputStream entradaPelSocket_en_flux;
        BufferedOutputStream bos = null;


        // Si inicialitzemoperacio és que el client vol enviar un fitxer al server.
        if (inicialitzemOperacio) {
            sortidaCapAlSocket_en_caracters.println("INICIALITZAR_FTP");
            sortidaCapAlSocket_en_caracters.flush();

            // Si no fessim les següents 2 línies, que no serveixen per a res, no faria falta que el server fes un retornControl().
            missatgeRebut = entradaPelSocket_en_caracters.readLine();
            System.out.println("CLIENT.ftp(): missatge rebut del server: " + missatgeRebut);

            // Enviem el nom del fitxer:
            sortidaCapAlSocket_en_caracters.println(FILE_TO_SEND);
            sortidaCapAlSocket_en_caracters.flush();

            // Enviem el tamany del fitxer:
            File myFile = new File("dirClient/" + FILE_TO_SEND);
            sortidaCapAlSocket_en_caracters.println(myFile.length());
            sortidaCapAlSocket_en_caracters.flush();

            // Enviem el contingut del fitxer:
            byte[] mybytearray = new byte[1024 * 16];        // Fem un array on entraran fins a 1024*16 bytes (1 byte = 1 caràcter del fitxer).

            try {
                //LI FEM AQUEST SLEEP PERQUE SINÒ MOLTES VEGADES CASCA L'ARRIBADA DE LA INFO AL SERVER.
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // LLegim el fitxer que volem enviar en blocs de tamany 1024*16 i els anem enviant:
                bis = new BufferedInputStream(new FileInputStream(myFile));
                sortidaCapAlSocket_en_flux = socket.getOutputStream();

                System.out.println("FTP transmetent... " + FILE_TO_SEND + "(" + myFile.length() + " bytes)");
                int readLength = -1;
                int i = 1;
                int numBytesEnviats = 0;

                while ((readLength = bis.read(mybytearray)) > 0) {
                    sortidaCapAlSocket_en_flux.write(mybytearray, 0, readLength);
                    sortidaCapAlSocket_en_flux.flush();

                    numBytesEnviats = numBytesEnviats + readLength;

                    System.out.println(i + ": Enviat " + readLength + " bytes del fitxer.");
                    i++;
                }

                System.out.println("Fi d'enviatment de fitxer. Enviats " + numBytesEnviats + " bytes.");

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bis != null) bis.close();
            }

            missatgeRebut = entradaPelSocket_en_caracters.readLine();
            System.out.println("CLIENT.fpt(): missatge rebut del server: " + missatgeRebut);

        } else {
            // Si NO inicialitzemoperacio és que el server envia un fitxer al client.

            retornControl(false, sortidaCapAlSocket_en_caracters);        // Retornem al client perquè pugui començar el ftp.

            // Rebem el nom del fitxer:
            FILE_TO_RECEIVED = entradaPelSocket_en_caracters.readLine();
            FILE_TO_RECEIVED = "dirClient/" + FILE_TO_RECEIVED;

            int tamanyFitxer = Integer.parseInt(entradaPelSocket_en_caracters.readLine());

            System.out.println("Nom del fitxer a rebre: " + FILE_TO_RECEIVED);
            System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");

            System.out.println("FTP client rebent...");

            // Rebem el fitxer (podríem rebre diversos blocs de tamany 1024*16 bytes):
            byte[] mybytearray = new byte[1024 * 16];
            entradaPelSocket_en_flux = socket.getInputStream();
            bos = new BufferedOutputStream(new FileOutputStream(FILE_TO_RECEIVED));
            int count;
            int i = 1;
            int numBytesRebuts = 0;

            while ((count = entradaPelSocket_en_flux.read(mybytearray)) != -1) { //&& (mybytearray[count-1] != '\0')) {
                bos.write(mybytearray, 0, count);

                numBytesRebuts = numBytesRebuts + count;

                System.out.println(i + ": Rebut " + count + " bytes del fitxer, mybytearray.length = " + mybytearray.length + ", numBytesRebuts = " + numBytesRebuts);
                i++;

                if (numBytesRebuts == tamanyFitxer) {
                    break;
                }
            }

            bos.close();

            System.out.println("Nom del fitxer a rebre: " + FILE_TO_RECEIVED);
            System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");

            System.out.println("Fi de recepció de fitxer. Rebuts " + numBytesRebuts + " bytes.");

            sortidaCapAlSocket_en_caracters.println("FINALITZAT_FTP. Rebut " + numBytesRebuts + " bytes.");
            sortidaCapAlSocket_en_caracters.flush();
        }
    }

}
