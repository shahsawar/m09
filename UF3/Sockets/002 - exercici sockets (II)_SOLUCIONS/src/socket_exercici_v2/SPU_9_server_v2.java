package socket_exercici_v2;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Scanner;
import java.util.StringTokenizer;


import Pantalles.MenuConstructorPantalla;

public class SPU_9_server_v2 {
    private static boolean veureMenu = true;        // La fem static perquè sigui accesssible des de qualsevol mètode de la classe.
    // No volem que cada vegada que entrem en el mètode 'procesarMissatgesDelClient' s'inicialitzi.
    private static String nomAplicacio;
    private static ServerSocket serverSocketTmp;    // Si declarem aquesta variable local dins del mètode 'establirConnexioTmpViaSocket'
    // peta el programa.


    private static void tancarSocket(Socket clientSocket) {
        //Si falla el tancament no podem fer gaire cosa, només enregistrar el problema.

        //Tancament de tots els recursos.
        if ((clientSocket != null) && (!clientSocket.isClosed())) {
            try {
                if (!clientSocket.isInputShutdown()) {
                    clientSocket.shutdownInput();
                }
                if (!clientSocket.isOutputShutdown()) {
                    clientSocket.shutdownOutput();
                }
                clientSocket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    private static void respondreAPermisConnexio(PrintWriter sortidaCapAlClientSocket_en_caracters) {
        Scanner sc = new Scanner(System.in);
        String resposta;


        System.out.print("Permets la connexió del client? (S/N) ");
        resposta = sc.next();

        if (resposta.equalsIgnoreCase("S")) {
            sortidaCapAlClientSocket_en_caracters.println("PERMIS_CONNEXIO_CONCEDIT");
        } else {
            sortidaCapAlClientSocket_en_caracters.println("PERMIS_CONNEXIO_DENEGAT");
        }
        sortidaCapAlClientSocket_en_caracters.flush();
    }


    private static void chat(boolean inicialitzemOperacio, BufferedReader entradaPelClientSocket_en_caracters, PrintWriter sortidaCapAlClientSocket_en_caracters) {
        String missatgeAEnviar = "";
        String missatgeRebut = "";
        Scanner sc2 = new Scanner(System.in);
        boolean acabarChat = false;


        if (inicialitzemOperacio) {
            // Inicialitzem el chat.
            sortidaCapAlClientSocket_en_caracters.println("INICIALITZAR_CHAT");
            sortidaCapAlClientSocket_en_caracters.flush();
        } else {
            // NO inicialitzem el chat.
            // Retornem al client perquè pugui començar el chat.
            retornControl(false, sortidaCapAlClientSocket_en_caracters);
        }

        try {
            do {
                missatgeRebut = entradaPelClientSocket_en_caracters.readLine();
                System.out.println(nomAplicacio + ": missatge rebut: " + missatgeRebut);

                if (missatgeRebut.equalsIgnoreCase("FINALITZAR_CHAT")) {
                    acabarChat = true;

                    if (!inicialitzemOperacio) {
                        // Retornem al client el control de les comunicacions.
                        retornControl(false, sortidaCapAlClientSocket_en_caracters);
                    }
                } else {
                    System.out.print(nomAplicacio + ": missatge a enviar: ");
                    missatgeAEnviar = sc2.nextLine();

                    if (missatgeAEnviar.equalsIgnoreCase("FINALITZAR_CHAT")) {
                        acabarChat = true;
                    }

                    sortidaCapAlClientSocket_en_caracters.println(missatgeAEnviar);
                    sortidaCapAlClientSocket_en_caracters.flush();
                }

            } while (acabarChat == false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void retornControl(boolean inicialitzemOperacio, PrintWriter sortidaCapAlSocket_en_caracters) {
        sortidaCapAlSocket_en_caracters.println("RETORN_CONTROL");
        sortidaCapAlSocket_en_caracters.flush();
    }


    private static void ftp(boolean inicialitzemOperacio, int portFtp, Socket socket, BufferedReader entradaPelSocket_en_caracters, PrintWriter sortidaCapAlSocket_en_caracters) {
        String FILE_TO_SEND = "battlestar galactica.bmp";
        BufferedInputStream bis = null;
        OutputStream sortidaCapAlSocket_en_flux = null;
        String missatgeRebut = "";
        String FILE_TO_RECEIVED;
        InputStream entradaPelSocket_en_flux;
        BufferedOutputStream bos = null;
        Socket socketFTP = null;


        try {
            // Si inicialitzemoperacio és que el volem enviar un fitxer.
            if (inicialitzemOperacio) {
                sortidaCapAlSocket_en_caracters.println("INICIALITZAR_FTP");
                sortidaCapAlSocket_en_caracters.flush();

                // Si no fessim les següents 2 línies, que no serveixen per a res, no faria falta que el server fes un retornControl().
                missatgeRebut = entradaPelSocket_en_caracters.readLine();
                System.out.println(nomAplicacio + ".ftp(): missatge rebut: " + missatgeRebut);

                // Enviem el nom del fitxer:
                sortidaCapAlSocket_en_caracters.println(FILE_TO_SEND);
                sortidaCapAlSocket_en_caracters.flush();

                // Enviem el tamany del fitxer:
                File myFile = new File(IKSRotarranConstants.PATH_DIR_CLIENT + FILE_TO_SEND);
                sortidaCapAlSocket_en_caracters.println(myFile.length());
                sortidaCapAlSocket_en_caracters.flush();


                // Obrim un socket al port del ftp per a transmetre el fitxer per aquest socket.
                System.out.println(nomAplicacio + ": creant nou socket de connexió...");
                socketFTP = establirConnexioTmpViaSocket(false, IKSRotarranConstants.PORT_FTP);
                System.out.println(nomAplicacio + ": creat nou socket de connexió.");


                // Enviem el contingut del fitxer:
                byte[] mybytearray = new byte[1024 * 16];        // Fem un array on entraran fins a 1024*16 bytes (1 byte = 1 caràcter del fitxer).

                // LI FEM AQUEST SLEEP PERQUE SINÒ MOLTES VEGADES CASCA L'ARRIBADA DE LA INFO AL SERVER.
                Thread.sleep(001);

                // LLegim el fitxer que volem enviar en blocs de tamany 1024*16 i els anem enviant:
                bis = new BufferedInputStream(new FileInputStream(myFile));
                sortidaCapAlSocket_en_flux = socketFTP.getOutputStream();

                System.out.println("FTP transmetent... " + FILE_TO_SEND + "(" + myFile.length() + " bytes)");
                int readLength = -1;
                int i = 1;
                int numBytesEnviats = 0;

                while ((readLength = bis.read(mybytearray)) > 0) {
                    sortidaCapAlSocket_en_flux.write(mybytearray, 0, readLength);
                    sortidaCapAlSocket_en_flux.flush();

                    numBytesEnviats = numBytesEnviats + readLength;

                    System.out.println(i + ": Enviat " + readLength + " bytes del fitxer.");
                    i++;
                }

                sortidaCapAlSocket_en_flux.close();
                tancarSocket(socketFTP);
                bis.close();

                System.out.println("Fi d'enviatment de fitxer. Enviats " + numBytesEnviats + " bytes.");

                missatgeRebut = entradaPelSocket_en_caracters.readLine();
                System.out.println(nomAplicacio + ".fpt(): missatge rebut: " + missatgeRebut);

            } else {
                // Si NO inicialitzem operacio és que rebrem un fitxer.

                retornControl(false, sortidaCapAlSocket_en_caracters);        // Retornem al client perquè pugui començar el ftp.

                // Rebem el nom del fitxer:
                FILE_TO_RECEIVED = entradaPelSocket_en_caracters.readLine();
                FILE_TO_RECEIVED = IKSRotarranConstants.PATH_DIR_SERVIDOR + FILE_TO_RECEIVED;

                // Rebem el tamany del fitxer:
                int tamanyFitxer = Integer.parseInt(entradaPelSocket_en_caracters.readLine());

                System.out.println("Nom del fitxer a rebre: " + FILE_TO_RECEIVED);
                System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");


                // Obrim un socket al port del ftp per a transmetre el fitxer per aquest socket.
                System.out.println(nomAplicacio + ": creant nou socket de connexió...");
                socketFTP = establirConnexioTmpViaSocket(true, IKSRotarranConstants.PORT_FTP);
                System.out.println(nomAplicacio + ": creat nou socket de connexió.");


                // Rebem el fitxer (podríem rebre diversos blocs de tamany 1024*16 bytes):
                byte[] mybytearray = new byte[1024 * 16];
                entradaPelSocket_en_flux = socketFTP.getInputStream();
                bos = new BufferedOutputStream(new FileOutputStream(FILE_TO_RECEIVED));
                int count;
                int i = 1;
                int numBytesRebuts = 0;

                System.out.println("FTP server rebent...");
                while ((count = entradaPelSocket_en_flux.read(mybytearray)) != -1) { //&& (mybytearray[count-1] != '\0')) {
                    bos.write(mybytearray, 0, count);

                    numBytesRebuts = numBytesRebuts + count;

                    System.out.println(i + ": Rebut " + count + " bytes del fitxer, mybytearray.length = " + mybytearray.length + ", numBytesRebuts = " + numBytesRebuts);
                    i++;

                    if (numBytesRebuts == tamanyFitxer) {
                        break;
                    }
                }

                entradaPelSocket_en_flux.close();
                tancarSocket(socketFTP);
                bos.close();

                System.out.println("Nom del fitxer a rebre: " + FILE_TO_RECEIVED);
                System.out.println("Tamany del fitxer a rebre: " + tamanyFitxer + " bytes.");
                System.out.println("Fi de recepció de fitxer. Rebuts " + numBytesRebuts + " bytes.");

                sortidaCapAlSocket_en_caracters.println("FINALITZAT_FTP. Rebut " + numBytesRebuts + " bytes.");
                sortidaCapAlSocket_en_caracters.flush();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (bis != null)
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }


    private static Socket establirConnexioTmpViaSocket(boolean actuaComServidor, int portConnexio) {
        //ServerSocket serverSocketTmp = null;
        Socket socketConnexioTmp = null;


        if (actuaComServidor) {
            try {
                if (serverSocketTmp != null) {
                    serverSocketTmp.close();
                    serverSocketTmp = null;
                }
                serverSocketTmp = new ServerSocket(portConnexio);
                socketConnexioTmp = serverSocketTmp.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            do {
                try {
                    // Amb aquest Thread.sleep() li donem temps a la versió server per arrancar el 'socketConnexio = serverSocketTmp.accept();'.
                    Thread.sleep(500);
                    SocketAddress sockaddr = new InetSocketAddress(IKSRotarranConstants.ADDRESS_SPU9, portConnexio);

                    socketConnexioTmp = new Socket();
                    socketConnexioTmp.connect(sockaddr);

                    System.out.println("1. socketConnexio.isConnected() = " + socketConnexioTmp.isConnected());
                } catch (IOException e) {
                    System.out.println("2. socketConnexio.isConnected() = " + socketConnexioTmp.isConnected());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (!socketConnexioTmp.isConnected());
        }

        return socketConnexioTmp;
    }


    private static boolean procesarMissatgesDelClient(Socket clientSocket, String missatgeDelClient, BufferedReader entradaPelClientSocket_en_caracters, PrintWriter sortidaCapAlClientSocket_en_caracters) {
        String opcio;
        Scanner sc = new Scanner(System.in);
        StringBuilder menu = new StringBuilder("");
        //String tipusMissatge = "";
        //StringTokenizer st;
        boolean acabarConnexioAmbElClient = false;


        System.out.println(nomAplicacio + ": rebut el codi " + missatgeDelClient + ".");

        // Aquest codi és per quan el client talla la comunicació, al cridar tancarSocket() s'envia 
        // automàticament un NULL pel socket que ens arriba aquí.
        if (missatgeDelClient == null) {
            System.out.println("EL CLIENT HA TALLAT LA COMUNICACIÓ.");
            missatgeDelClient = "EL_CLIENT_HA_TALLAT_LA_COMUNICACIO";
            acabarConnexioAmbElClient = true;
        }


        veureMenu = false;

        // El client és qui inicialitza la comunicació i per tant enviarà un 'DEMANAR_PERMIS_CONNEXIO'.
        if (missatgeDelClient.equals("DEMANAR_PERMIS_CONNEXIO")) {
            respondreAPermisConnexio(sortidaCapAlClientSocket_en_caracters);
        }

        if (missatgeDelClient.equals("RETORN_CONTROL")) {
            veureMenu = true;
        }

        if (missatgeDelClient.equals("INICIALITZAR_CHAT")) {
            chat(false, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);
            System.out.println();
        }

        if (missatgeDelClient.equals("INICIALITZAR_FTP")) {
            ftp(false, IKSRotarranConstants.PORT_FTP, clientSocket, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);
            System.out.println();
        }


        if (veureMenu == true) {
            do {
                menu.delete(0, menu.length());

                menu.append(System.getProperty("line.separator"));
                menu.append("SPU-9 SERVER ");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));

                menu.append("0. Desconnectar-se del CLIENT");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));
                menu.append("1. Retornar el control de les comunicacions al CLIENT");
                menu.append(System.getProperty("line.separator"));
                menu.append("2. CHAT");
                menu.append(System.getProperty("line.separator"));
                menu.append("3. FTP");
                menu.append(System.getProperty("line.separator"));
                menu.append(System.getProperty("line.separator"));

                menu.append("50. Tancar el sistema");
                menu.append(System.getProperty("line.separator"));

                System.out.print(MenuConstructorPantalla.constructorPantalla(menu));

                opcio = sc.next();

                switch (opcio) {
                    case "0":
                        acabarConnexioAmbElClient = true;
                        veureMenu = false;
                        break;
                    case "1":
                        retornControl(true, sortidaCapAlClientSocket_en_caracters);
                        veureMenu = false;
                        break;
                    case "2":
                        chat(true, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);
                        System.out.println();
                        break;
                    case "3":
                        ftp(true, IKSRotarranConstants.PORT_FTP, clientSocket, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);
                        System.out.println();
                        break;
                    case "50":
                        acabarConnexioAmbElClient = true;
                        veureMenu = false;
                        break;
                    default:
                        System.out.println("COMANDA NO RECONEGUDA");
                }
            } while ((!opcio.equals("50")) && (veureMenu == true) && (acabarConnexioAmbElClient == false));
        }

        return acabarConnexioAmbElClient;
    }


    private static void procesarComunicacionsAmbClient(Socket clientSocket) {
        String missatgeDelClient = "";
        BufferedReader entradaPelClientSocket_en_caracters = null;
        PrintWriter sortidaCapAlClientSocket_en_caracters = null;
        boolean acabarComunicacio = false;


        try {
            // Obrim entrada i sortida per caràcters en el socket.
            entradaPelClientSocket_en_caracters = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            sortidaCapAlClientSocket_en_caracters = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);

            // Processa la petició de connexió del client. El client és qui inicialitza la connexió.
            do {
                missatgeDelClient = entradaPelClientSocket_en_caracters.readLine();

                acabarComunicacio = procesarMissatgesDelClient(clientSocket, missatgeDelClient, entradaPelClientSocket_en_caracters, sortidaCapAlClientSocket_en_caracters);

            } while (acabarComunicacio == false);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    private static void escoltar() {
        boolean fiComunicacio = false;
        ServerSocket serverSocket = null;
        Socket clientSocket = null;


        try {
            // Es crea un ServerSocket que atendrà el port nº PORT a l'espera de clients que demanin comunicar-se.
            serverSocket = new ServerSocket(IKSRotarranConstants.PORT_SPU_9);

            while (!fiComunicacio) {
                // El mètode accept() resta a l'espera d'una petició i en el moment de produir-se crea una instància
                // específica de sòcol per suportar la comunicació amb el client acceptat.
                // El client és qui inicialitza la comunicació.
                clientSocket = serverSocket.accept();

                // Processem la petició del client.
                procesarComunicacionsAmbClient(clientSocket);
                // SEMBLA QUE AQUEST SERVER NOMÉS POT TREBALLAR AMB 1 CLIENT A L'HORA.
                // Quan es desconnecti el client, esperarà a que es connecti un altre i per tant el server no s'aturarà mai.

                // Tanquem el sòcol temporal que vam obrir per atendre al client.
                tancarSocket(clientSocket);

                System.out.println("SERVER.escoltar(): el client s'ha desconnectat, esperant a que es connecti un altre client...");
            }

            // Tanquem el sòcol principal
            if ((serverSocket != null) && (!serverSocket.isClosed())) {
                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Thread.currentThread().setName("SPU-9 Server");
        nomAplicacio = Thread.currentThread().getName();

        System.out.println(nomAplicacio + " - INICI");

        escoltar();

        System.out.println("SPU-9 Server - FI");
    }

}
