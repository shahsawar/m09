package socket_exercici_v2;

public interface IKSRotarranConstants {
	String PATH_DIR_CLIENT = "dirClient/";
	String PATH_DIR_SERVIDOR = "dirServidor/";

	String ADDRESS_SPU9 = "127.0.0.1";
	
	int PORT_SPU_9 = 9090;
	int PORT_CHAT = 9091;
	int PORT_FTP = 9092;
}
