public class Exercici_2_v6 {

    public static void inicialitzarPrograma(){

        SistemaDeGuiaDeTorpedes sistemaDeGuiaDeTorpedes = new SistemaDeGuiaDeTorpedes(3);
        Thread[] torpedes = new Thread[10];

        sistemaDeGuiaDeTorpedes.imprimirUsDelSistemaDeGuia("Exercici_7.inicialitzarPrograma() - INICI");
        for (int i = 0; i < torpedes.length; i++) {
            Torpede torpedeTmp = new Torpede(sistemaDeGuiaDeTorpedes);
            torpedes[i] = new Thread(torpedeTmp);
            torpedes[i].setName("torpede "+ i);
            torpedes[i].start();
        }
        try {
            torpedes[torpedes.length -1].join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sistemaDeGuiaDeTorpedes.imprimirUsDelSistemaDeGuia("Exercici_7.inicialitzarPrograma() - FI");
    }
}
