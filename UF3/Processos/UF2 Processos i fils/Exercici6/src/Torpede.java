public class Torpede implements Runnable{

    private SistemaDeGuiaDeTorpedes sistemaDeGuiaDeTorpedes;

    public Torpede(SistemaDeGuiaDeTorpedes sistemaDeGuiaDeTorpedes){
        this.sistemaDeGuiaDeTorpedes = sistemaDeGuiaDeTorpedes;
    }

    @Override
    public void run() {
        int numUsDelSistemaDeGuia;
        System.out.println(Thread.currentThread().getName() + ".INICI");
        numUsDelSistemaDeGuia = sistemaDeGuiaDeTorpedes.adquirirSistemaDeGuia(Thread.currentThread().getName());
        try {
            Thread.currentThread().sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sistemaDeGuiaDeTorpedes.alliberarSistemaDeGuia(numUsDelSistemaDeGuia, Thread.currentThread().getName() + ".FI");
    }
}
