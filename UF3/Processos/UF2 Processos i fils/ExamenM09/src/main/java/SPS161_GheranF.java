public class SPS161_GheranF implements Runnable{

    //Objecta sirena3M
    SPO10_Sirena3M sirena3M = new SPO10_Sirena3M();

    //Constructor
    public SPS161_GheranF(SPO10_Sirena3M sirena3M ){
        this.sirena3M = sirena3M;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + ": INICI");
        try {
            Thread.currentThread().sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + ": no puc despistar al torpede enemic");
        sirena3M.setDespistaremTorpede(false);
        System.out.println(Thread.currentThread().getName() + ": FI");
    }
}
