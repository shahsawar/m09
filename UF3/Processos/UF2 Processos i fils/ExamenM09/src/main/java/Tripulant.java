import java.util.concurrent.Semaphore;

public class Tripulant implements Runnable {

    //Atributs
    int id;
    SPO10_Sirena3M sirena3M;

    //Constructor
    public Tripulant(int id, SPO10_Sirena3M sirena3M) {
        this.id = id;
        this.sirena3M = sirena3M;
    }

    @Override
    public void run() {
        boolean consultAbandNau = sirena3M.consultarAbandonamentNau();

        //Si li retorna un false a llavors, abans de tornar a executar el mètode, feu un sleep(4000) del tripulant
        while (consultAbandNau == false) {
            try {
                Thread.currentThread().sleep(4000);
                consultAbandNau = sirena3M.consultarAbandonamentNau();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //Quan li retorni true a llavors crideu al mètode demanarCapsulaSalvaVides() de SPO-10 Sirena-3M.
        if (consultAbandNau) {
            sirena3M.demanarCapsulaSalvaVides(this.id);
        }
    }
}
