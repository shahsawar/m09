public class SimularCombat {

    public static void main(String[] args) {

        //Fils sirena3M
        SPO10_Sirena3M sirena3M = new SPO10_Sirena3M();
        Thread filSirena3M = new Thread(sirena3M);
        filSirena3M.setName("SPO-10 Sirena-3M");
        filSirena3M.start();

        //Fils Tripulants
        Thread[] tripulants = new Thread[36];
        for (int i = 0; i < tripulants.length; i++) {
            Tripulant tripulantTmp = new Tripulant(i + 1, sirena3M);
            tripulants[i] = new Thread(tripulantTmp);
            tripulants[i].setName("Tripulant " + i);
            tripulants[i].start();
        }

        //No s'ha d'acabar l'execució fins que no acabi d'executar-se el fil de SPO-10 Sirena-3M.
        try {
            filSirena3M.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
