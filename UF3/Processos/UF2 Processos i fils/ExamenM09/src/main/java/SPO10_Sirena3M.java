import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class SPO10_Sirena3M implements Runnable {

    //Atributs
    private ArrayList<ArrayList<Integer>> usDeLesCapsulesSalvaVides;
    private boolean despistaremTorpede;
    private boolean abandonarLaNau;
    private Semaphore semaphoreForGheranf;
    private Semaphore semaphoreForGherantFNoDesviat;
    private Semaphore semaphoreForAllTripulants;

    //Constructor
    public SPO10_Sirena3M() {
        usDeLesCapsulesSalvaVides = new ArrayList<>();
        usDeLesCapsulesSalvaVides.add(new ArrayList<Integer>());
        usDeLesCapsulesSalvaVides.add(new ArrayList<Integer>());
        usDeLesCapsulesSalvaVides.add(new ArrayList<Integer>());
        usDeLesCapsulesSalvaVides.add(new ArrayList<Integer>());
        despistaremTorpede = true;
        abandonarLaNau = false;
        semaphoreForGheranf = new Semaphore(1);
        semaphoreForGherantFNoDesviat = new Semaphore(1);
        semaphoreForAllTripulants = new Semaphore(36);
    }

    //Getters and Setters
    public boolean isDespistaremTorpede() {
        return despistaremTorpede;
    }

    public void setDespistaremTorpede(boolean despistaremTorpede) {
        this.despistaremTorpede = despistaremTorpede;
    }

    public boolean isAbandonarLaNau() {
        return abandonarLaNau;
    }

    public void setAbandonarLaNau(boolean abandonarLaNau) {
        this.abandonarLaNau = abandonarLaNau;
    }

    public ArrayList<ArrayList<Integer>> getUsDeLesCapsulesSalvaVides() {
        return usDeLesCapsulesSalvaVides;
    }

    public void setUsDeLesCapsulesSalvaVides(ArrayList<ArrayList<Integer>> usDeLesCapsulesSalvaVides) {
        this.usDeLesCapsulesSalvaVides = usDeLesCapsulesSalvaVides;
    }

    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName() + ": INICI");
        System.out.println(Thread.currentThread().getName() + ": detectat torpede enemic...");
        System.out.println(Thread.currentThread().getName() + ": activant el interferidor de radar actiu SPS-161 Gheran-F");

        try {
            semaphoreForGheranf.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Executar un fil de SPS-161 Gheran-F per intentar desviar al torpede enemic.
        SPS161_GheranF objgheranF = new SPS161_GheranF(this);
        Thread gheranf = new Thread(objgheranF);
        gheranf.setName("SPS-161 Gheran-F");
        gheranf.start();

        //Ha d'esperar fins que SPS-161 Gheran-F l'informi si pot o no pot desviar el torpede.
        try {
            gheranf.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        semaphoreForGheranf.release();

        //Si es pot desviar el torpede enemic (l'atribut despistaremTorpedetindrà un true) traurà per pantalla
        // un missatge dient que SPS-161 Gherant-F ha desviat el torpede i no hi ha perill i s'haurà acabat
        // l'execucióde SPO-10 Sirena-3M.
        if (despistaremTorpede) {
            System.out.println("SPS-161 Gherant-F ha desviat el torpede i no hi ha perill");
            System.out.println("SPO-10 Sirena3M FI");
        } else {
            try {
                semaphoreForGherantFNoDesviat.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            despistaremTorpede = false;
            abandonarLaNau = true;

            System.out.println("llançament de les 4 càpsules...");
        }
    }

    public boolean consultarAbandonamentNau() {
        return abandonarLaNau;
    }

    public void demanarCapsulaSalvaVides(int numTripulant) {
        Random rand = new Random();
        try {
            Thread.currentThread().sleep(rand.nextInt(500 - 1 + 1) + 1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Insertar el tripulants en las llistas
        if (usDeLesCapsulesSalvaVides.get(0).size() < 9) {
            usDeLesCapsulesSalvaVides.get(0).add(numTripulant);
            System.out.println("Tripulant " + numTripulant + ", espai disponible en la càpsula 1 = " + (9 - checkAvailableSpace(0)));

        } else if (usDeLesCapsulesSalvaVides.get(1).size() < 9) {
            usDeLesCapsulesSalvaVides.get(1).add(numTripulant);
            System.out.println("Tripulant " + numTripulant + ", espai disponible en la càpsula 2 = " + (9 - checkAvailableSpace(1)));

        } else if (usDeLesCapsulesSalvaVides.get(2).size() < 9) {
            usDeLesCapsulesSalvaVides.get(2).add(numTripulant);
            System.out.println("Tripulant " + numTripulant + ", espai disponible en la càpsula 3 = " + (9 - checkAvailableSpace(2)));

        } else if (usDeLesCapsulesSalvaVides.get(3).size() < 9) {
            usDeLesCapsulesSalvaVides.get(3).add(numTripulant);
            System.out.println("Tripulant " + numTripulant + ", espai disponible en la càpsula 4 = " + (9 - checkAvailableSpace(3)));
        }

        //Mostrar el contingut de tots els arrays.
        if (usDeLesCapsulesSalvaVides.get(3).size() == 9) {
            System.out.println("MAIN: assignació de càpsules:");
            System.out.println(usDeLesCapsulesSalvaVides.get(0).toString());
            System.out.println(usDeLesCapsulesSalvaVides.get(1).toString());
            System.out.println(usDeLesCapsulesSalvaVides.get(2).toString());
            System.out.println(usDeLesCapsulesSalvaVides.get(3).toString());
        }
    }

    public int checkAvailableSpace(int arrayNum) {
        return usDeLesCapsulesSalvaVides.get(arrayNum).size() - 1;
    }
}
