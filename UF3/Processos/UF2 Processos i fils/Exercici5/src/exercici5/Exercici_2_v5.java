package exercici5;

public class Exercici_2_v5 {

    public static void inicialitzarPrograma() throws InterruptedException {

        MagatzemCombustible_v5 magatzemCombustibleV1 = new MagatzemCombustible_v5();
        DeptCienciaProductor_v5 deptCienciaProductor = new DeptCienciaProductor_v5(magatzemCombustibleV1);
        DeptEnginyeriaConsumidor_v5 deptEnginyeriaConsumidor = new DeptEnginyeriaConsumidor_v5(magatzemCombustibleV1, 7);
        DeptEnginyeriaConsumidor_v5 deptEnginyeriaConsumidor2 = new DeptEnginyeriaConsumidor_v5(magatzemCombustibleV1, 10);

        System.out.println("Exercici_2.inicialitzarPrograma() - INICI");
        System.out.println("Exercici_2.magatzemDeCombustible.posicionsEnMagatzem = " + magatzemCombustibleV1.posiconsEnMagatzem.toString());

        Thread cienciaProductor = new Thread(deptCienciaProductor);
        Thread enginyeriaConsumidor = new Thread(deptEnginyeriaConsumidor);
        enginyeriaConsumidor.setName("Enginyeria Consumidor 1");
        Thread enginyeriaConsumidor2 = new Thread(deptEnginyeriaConsumidor2);
        enginyeriaConsumidor2.setName("Enginyeria Consumidor 2");

        cienciaProductor.start();
        enginyeriaConsumidor.start();
        enginyeriaConsumidor2.start();

        cienciaProductor.join(5000);
        enginyeriaConsumidor.join(5000);
        enginyeriaConsumidor2.join(5000);


        System.out.println("Exercici_2.magatzemDeCombustible.posicionsEnMagatzem = " + magatzemCombustibleV1.posiconsEnMagatzem.toString());
        System.out.println("Exercici_2.inicialitzarPrograma() - FI");

    }
}
