package exercici5;

public class DeptEnginyeriaConsumidor_v5 implements Runnable {

    MagatzemCombustible_v5 magatzemDeConsumidor = new MagatzemCombustible_v5();
    private int numContenidor;

    public DeptEnginyeriaConsumidor_v5(MagatzemCombustible_v5 magatzemDeConsumidor, int num) {
        this.magatzemDeConsumidor = magatzemDeConsumidor;
        this.numContenidor = num;
    }

    @Override
    public void run() {
        int i = 0;
        boolean exitResult;

        System.out.println("2222 - DeptEnginyeriaConsumidor.INICI");
        while (i < numContenidor) {
            exitResult = magatzemDeConsumidor.consumirContenidorDeCombustible();
            if (exitResult)
                i++;
        }
        System.out.println("2222 - DeptEnginyeriaConsumidor.FI");
    }
}
