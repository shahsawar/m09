package exercici5;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class MagatzemCombustible_v5 {

    ArrayList<Character> posiconsEnMagatzem = new ArrayList<>();
    private Semaphore semaphore;

    public MagatzemCombustible_v5() {
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        semaphore = new Semaphore(1);
    }

    public synchronized int numContenidorsAlMagatzem() {
        int numContenidorTmp = 0;
        for (char mtmp : this.posiconsEnMagatzem) {
            if (mtmp == '1')
                numContenidorTmp++;
        }
        return numContenidorTmp;
    }


    public synchronized Boolean produirContenidorDeCombustible() {
        boolean exitResult = false;
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (numContenidorsAlMagatzem() < 10) {
            System.out.println("1111.1 - DeptCienciaProductor, numContenidorsAlMagatzem() = " + numContenidorsAlMagatzem() + ", " + posiconsEnMagatzem);
            int posTmp = posiconsEnMagatzem.indexOf('0');
            posiconsEnMagatzem.set(posTmp, '1');
            exitResult = true;
        }
        System.out.println("1111.2 - DeptCienciaProductor, numContenidorsAlMagatzem() = " + numContenidorsAlMagatzem() + ", " + posiconsEnMagatzem + ", exitOperacio = " + exitResult);
        semaphore.release();
        return exitResult;
    }


    public synchronized Boolean consumirContenidorDeCombustible() {
        boolean exitResult = false;
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (numContenidorsAlMagatzem() > 0) {
            System.out.println("2222.1 - DeptEnginyeriaConsumidor, numContenidorsAlMagatzem() = " + numContenidorsAlMagatzem() + ", " + posiconsEnMagatzem);
            int posTmp = posiconsEnMagatzem.indexOf('1');
            posiconsEnMagatzem.set(posTmp, '0');
            exitResult = true;
        }
        System.out.println("2222.2 - DeptEnginyeriaConsumidor, numContenidorsAlMagatzem() = " + numContenidorsAlMagatzem() + ", " + posiconsEnMagatzem + ", exitOperacio = " + exitResult);
        semaphore.release();
        return exitResult;
    }

}
