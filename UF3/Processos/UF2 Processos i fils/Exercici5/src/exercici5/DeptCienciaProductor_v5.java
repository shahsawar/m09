package exercici5;

public class DeptCienciaProductor_v5 implements Runnable {

    private MagatzemCombustible_v5 magatzemDeCombustible;

    public DeptCienciaProductor_v5(MagatzemCombustible_v5 magatzemDeCombustible) {
        this.magatzemDeCombustible = magatzemDeCombustible;
    }

    @Override
    public void run() {
        int i = 0;
        boolean exitResult;
        System.out.println("1111 - DeptCienciaProductor.INICI");
        while (i < 20) {
            exitResult = magatzemDeCombustible.produirContenidorDeCombustible();
            if (exitResult){
                i++;
            }
        }
        System.out.println("1111 - DeptCienciaProductor.FI");
    }
}
