package exercici3;

public class DeptCienciaProductor_v3 implements Runnable {

    private MagatzemCombustible_v3 magatzemDeCombustible;

    public DeptCienciaProductor_v3(MagatzemCombustible_v3 magatzemDeCombustible) {
        this.magatzemDeCombustible = magatzemDeCombustible;
    }

    @Override
    public void run() {
        int i = 0;
        System.out.println("1111 - DeptCienciaProductor.INICI");
        while (i < 20) {
            int numContenidorTmp = magatzemDeCombustible.numContenidorsAlMagatzem();
            if ( numContenidorTmp < 10) {
                System.out.println("1111.1 - DeptCienciaProductor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeCombustible.posiconsEnMagatzem.toString());
                magatzemDeCombustible.produirContenidorDeCombustible();
                System.out.println("1111.2 - DeptCienciaProductor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeCombustible.posiconsEnMagatzem.toString());
                i++;
            }
        }
        System.out.println("1111 - DeptCienciaProductor.FI");
    }
}
