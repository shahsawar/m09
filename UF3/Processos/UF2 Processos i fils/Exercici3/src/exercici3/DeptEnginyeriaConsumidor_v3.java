package exercici3;

public class DeptEnginyeriaConsumidor_v3 implements Runnable {

    MagatzemCombustible_v3 magatzemDeConsumidor = new MagatzemCombustible_v3();

    public DeptEnginyeriaConsumidor_v3(MagatzemCombustible_v3 magatzemDeConsumidor) {
        this.magatzemDeConsumidor = magatzemDeConsumidor;
    }

    @Override
    public void run() {
        int i = 0;
        System.out.println("2222 - DeptEnginyeriaConsumidor.INICI");

        i = 0;
        while (i < 13) {
            int numContenidorTmp = magatzemDeConsumidor.numContenidorsAlMagatzem();
            if (magatzemDeConsumidor.numContenidorsAlMagatzem() > 0) {
                System.out.println("2222.1 - [" + i + "] - DeptEnginyeriaConsumidor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeConsumidor.posiconsEnMagatzem.toString());
                magatzemDeConsumidor.consumirContenidorDeCombustible();
                System.out.println("2222.2 - [" + i + "] - DeptEnginyeriaConsumidor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeConsumidor.posiconsEnMagatzem.toString());
                i++;
            }
        }
        System.out.println("2222 - DeptEnginyeriaConsumidor.FI");
    }
}
