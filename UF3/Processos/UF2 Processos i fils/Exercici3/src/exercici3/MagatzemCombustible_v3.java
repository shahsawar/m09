package exercici3;

import java.util.ArrayList;

public class MagatzemCombustible_v3 {

    ArrayList<Character> posiconsEnMagatzem = new ArrayList<>();
    int posicio = -1;

    public MagatzemCombustible_v3() {
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
    }

    /*
    public synchronized int numContenidorsAlMagatzemV2() {
        int numContenidors;
        numContenidors = posicio + 1;
        return numContenidors;
    }*/

    public synchronized int numContenidorsAlMagatzem() {

        int numContenidorTmp = 0;
        for (char mtmp : this.posiconsEnMagatzem) {
            if (mtmp == '1')
                numContenidorTmp += 1;
        }
        return numContenidorTmp;
    }

    public synchronized void produirContenidorDeCombustible() {

        if (numContenidorsAlMagatzem() < 10) {
            int posTmp = posiconsEnMagatzem.indexOf('0');
            posiconsEnMagatzem.set(posTmp, '1');
        } else {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void consumirContenidorDeCombustible() {

        if (numContenidorsAlMagatzem() > 0) {
            int posTmp = posiconsEnMagatzem.indexOf('1');
            posiconsEnMagatzem.set(posTmp, '0');
        } else {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
