public class Exercici_1 {

    //This id is used for pause the first thread for 5 seconds
    public static long threadID;

    public static void inicialitzarPrograma() throws InterruptedException {

        Magatzem magatzem = new Magatzem();

        //Create objects exercici1.CapDeDepartament
        CapDeDepartament ojbCapDepartamentComandament = new CapDeDepartament(magatzem, 100);
        CapDeDepartament ojbCapDepartamentArmes = new CapDeDepartament(magatzem, 100);
        CapDeDepartament ojbCapDepartamentTimoINavegacio = new CapDeDepartament(magatzem, 30);
        CapDeDepartament ojbCapDepartamentEnginyeria = new CapDeDepartament(magatzem, 1000);
        CapDeDepartament ojbCapDepartamentCiencia = new CapDeDepartament(magatzem, 50);

        //Create the threads that represent capDeDepartments
        Thread threadcapDeDepartamentComandant = new Thread(ojbCapDepartamentComandament);
        threadcapDeDepartamentComandant.setName("Fil del departament 'commandament'");
        threadID = threadcapDeDepartamentComandant.getId();

        Thread threadcapDeDepartamentArmes = new Thread(ojbCapDepartamentArmes);
        threadcapDeDepartamentArmes.setName("Fil del departament 'armes'");

        Thread threadcapDeDepartamentTimoNavegacio = new Thread(ojbCapDepartamentTimoINavegacio);
        threadcapDeDepartamentTimoNavegacio.setName("Fil del departament 'timi i navegacio'");

        Thread threadcapDeDepartamentEnginyeria = new Thread(ojbCapDepartamentEnginyeria);
        threadcapDeDepartamentEnginyeria.setName("Fil del departament 'enginyeria'");

        Thread threadcapDeDepartamentCiencia = new Thread(ojbCapDepartamentCiencia);
        threadcapDeDepartamentCiencia.setName("Fil del departament 'ciencia'");

        //Start the thread that reperesent capDeDepartament
        threadcapDeDepartamentComandant.start();
        threadcapDeDepartamentArmes.start();
        threadcapDeDepartamentTimoNavegacio.start();
        threadcapDeDepartamentEnginyeria.start();
        threadcapDeDepartamentCiencia.start();

        //The main thread waits for the children to finish before continuing.
        threadcapDeDepartamentComandant.join();
        threadcapDeDepartamentArmes.join();
        threadcapDeDepartamentTimoNavegacio.join();
        threadcapDeDepartamentEnginyeria.join();
        threadcapDeDepartamentCiencia.join();

        System.out.println("Saldo total (desde el main) = " + magatzem.comprovarQuantitatRacions());
        System.out.println("Final Fil Principal");
    }
}
