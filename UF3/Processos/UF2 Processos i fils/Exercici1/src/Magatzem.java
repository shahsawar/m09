public class Magatzem {

    private int quantitatRacions = 1000;

    public Magatzem() {
    }

    public synchronized void retornarRacions(int numRacions){
        if (Thread.currentThread().getId() == Exercici_1.threadID) {
            try {
                System.out.println("\t" + Thread.currentThread().getName() + ".SLEEP");
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.quantitatRacions += numRacions;
    }

    public synchronized void agafaRacions(int numRacions){
        quantitatRacions -= numRacions;
    }

    public synchronized int comprovarQuantitatRacions(){
        return this.quantitatRacions;
    }
}
