public class CapDeDepartament implements Runnable {

    private Magatzem magatzem;
    int movimentRacions;

    public CapDeDepartament(Magatzem magatzem, int movimentRacions) {
        this.magatzem = magatzem;
        this.movimentRacions = movimentRacions;
    }

    @Override
    public void run() {
        String nomFil = Thread.currentThread().getName();

        System.out.println("\t" + nomFil + ".INICI");
        System.out.println("\t" + nomFil + ".movimentRacions = " + movimentRacions);
        System.out.println("\t" + nomFil + ".magatzem.comprovarQuantitatRacions() = " + magatzem.comprovarQuantitatRacions());
        System.out.println("\t" + nomFil + ".REALITZO L'OPERACIÓ");

        if (movimentRacions > 0) {
            magatzem.retornarRacions(movimentRacions);
        } else {
            magatzem.agafaRacions(movimentRacions);
        }

        System.out.println("\t" + nomFil + ".magatzem.comprovarQuantitatRacions() = " + magatzem.comprovarQuantitatRacions());
        System.out.println("\t" + nomFil + ".FI");
    }
}
