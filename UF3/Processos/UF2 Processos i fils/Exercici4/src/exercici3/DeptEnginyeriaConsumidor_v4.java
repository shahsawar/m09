package exercici3;

public class DeptEnginyeriaConsumidor_v4 implements Runnable {

    MagatzemCombustible_v4 magatzemDeConsumidor = new MagatzemCombustible_v4();

    public DeptEnginyeriaConsumidor_v4(MagatzemCombustible_v4 magatzemDeConsumidor) {
        this.magatzemDeConsumidor = magatzemDeConsumidor;
    }

    @Override
    public void run() {
        int i = 0;
        boolean exitResult;
        System.out.println("2222 - DeptEnginyeriaConsumidor.INICI");

        while (i < 13) {
            exitResult = magatzemDeConsumidor.consumirContenidorDeCombustible();
            if (exitResult)
                i++;
        }
        System.out.println("2222 - DeptEnginyeriaConsumidor.FI");
    }
}
