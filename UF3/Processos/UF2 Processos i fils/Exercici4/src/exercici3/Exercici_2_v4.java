package exercici3;

public class Exercici_2_v4 {

    public static void inicialitzarPrograma() throws InterruptedException {

        MagatzemCombustible_v4 magatzemCombustibleV1 = new MagatzemCombustible_v4();
        DeptCienciaProductor_v4 deptCienciaProductorV1 = new DeptCienciaProductor_v4(magatzemCombustibleV1);
        DeptEnginyeriaConsumidor_v4 deptEnginyeriaConsumidorV1 = new DeptEnginyeriaConsumidor_v4(magatzemCombustibleV1);

        System.out.println("Exercici_2.inicialitzarPrograma() - INICI");
        System.out.println("Exercici_2.magatzemDeCombustible.posicionsEnMagatzem = " + magatzemCombustibleV1.posiconsEnMagatzem.toString());

        Thread cienciaProductor = new Thread(deptCienciaProductorV1);
        Thread enginyeriaConsumidor = new Thread(deptEnginyeriaConsumidorV1);

        cienciaProductor.start();
        enginyeriaConsumidor.start();

        cienciaProductor.join(5000);
        enginyeriaConsumidor.join(5000);


        System.out.println("Exercici_2.magatzemDeCombustible.posicionsEnMagatzem = " + magatzemCombustibleV1.posiconsEnMagatzem.toString());
        System.out.println("Exercici_2.inicialitzarPrograma() - FI");

    }
}
