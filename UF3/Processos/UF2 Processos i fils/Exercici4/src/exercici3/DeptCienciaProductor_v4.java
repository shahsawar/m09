package exercici3;

public class DeptCienciaProductor_v4 implements Runnable {

    private MagatzemCombustible_v4 magatzemDeCombustible;

    public DeptCienciaProductor_v4(MagatzemCombustible_v4 magatzemDeCombustible) {
        this.magatzemDeCombustible = magatzemDeCombustible;
    }

    @Override
    public void run() {
        int i = 0;
        boolean exitResult;
        System.out.println("1111 - DeptCienciaProductor.INICI");
        while (i < 20) {
            exitResult = magatzemDeCombustible.produirContenidorDeCombustible();
            if (exitResult)
                i++;
        }
        System.out.println("1111 - DeptCienciaProductor.FI");
    }
}
