package exercici2_v2;

public class Exercici_2_v2 {

    public static void inicialitzarPrograma() throws InterruptedException {

        MagatzemCombustible_v2 magatzemCombustibleV1 = new MagatzemCombustible_v2();
        DeptCienciaProductor_v2 deptCienciaProductorV1 = new DeptCienciaProductor_v2(magatzemCombustibleV1);
        DeptEnginyeriaConsumidor_v2 enginyeriaConsumidorV1 = new DeptEnginyeriaConsumidor_v2(magatzemCombustibleV1);

        System.out.println("Exercici_2.inicialitzarPrograma() - INICI");
        System.out.println("Exercici_2.magatzemDeCombustible.posicionsEnMagatzem = " + magatzemCombustibleV1.posiconsEnMagatzem.toString());

        deptCienciaProductorV1.start();
        enginyeriaConsumidorV1.start();

        deptCienciaProductorV1.join();
        enginyeriaConsumidorV1.join();

        System.out.println("Exercici_2.magatzemDeCombustible.posicionsEnMagatzem = " + magatzemCombustibleV1.posiconsEnMagatzem.toString());
        System.out.println("Exercici_2.inicialitzarPrograma() - FI");

    }
}
