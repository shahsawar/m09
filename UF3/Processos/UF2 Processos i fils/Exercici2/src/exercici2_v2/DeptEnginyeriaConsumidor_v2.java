package exercici2_v2;

public class DeptEnginyeriaConsumidor_v2 extends Thread {

    MagatzemCombustible_v2 magatzemDeConsumidor = new MagatzemCombustible_v2();

    public DeptEnginyeriaConsumidor_v2(MagatzemCombustible_v2 magatzemDeConsumidor) {
        this.magatzemDeConsumidor = magatzemDeConsumidor;
    }

    @Override
    public void run() {
        int i = 0;
        System.out.println("2222 - DeptEnginyeriaConsumidor.INICI");

        i = 0;
        while (i < 13) {
            int numContenidorTmp = magatzemDeConsumidor.numContenidorsAlMagatzem();
            if (magatzemDeConsumidor.numContenidorsAlMagatzem() > 0) {
                System.out.println("2222.1 - [" + i + "] - DeptEnginyeriaConsumidor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeConsumidor.posiconsEnMagatzem.toString());
                magatzemDeConsumidor.consumirContenidorDeCombustible();
                System.out.println("2222.2 - [" + i + "] - DeptEnginyeriaConsumidor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeConsumidor.posiconsEnMagatzem.toString());
                i++;
            }
        }
        System.out.println("2222 - DeptEnginyeriaConsumidor.FI");
    }
}
