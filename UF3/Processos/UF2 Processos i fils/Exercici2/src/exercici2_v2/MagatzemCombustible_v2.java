package exercici2_v2;

import java.util.ArrayList;

public class MagatzemCombustible_v2 {

    ArrayList<Character> posiconsEnMagatzem = new ArrayList<>();
    int posicio = -1;

    public MagatzemCombustible_v2() {
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
        posiconsEnMagatzem.add('0');
    }

    /*
    public synchronized int numContenidorsAlMagatzemV2() {
        int numContenidors;
        numContenidors = posicio + 1;
        return numContenidors;
    }*/

    public synchronized int numContenidorsAlMagatzem() {

        int numContenidorTmp = 0;
        for (char mtmp : this.posiconsEnMagatzem) {
            if (mtmp == '1')
                numContenidorTmp += 1;
        }
        return numContenidorTmp;
    }

    public void produirContenidorDeCombustible() {
        int posTmp = posiconsEnMagatzem.indexOf('0');
        posiconsEnMagatzem.set(posTmp, '1');
    }

    public void consumirContenidorDeCombustible() {
        int posTmp = posiconsEnMagatzem.indexOf('1');
        posiconsEnMagatzem.set(posTmp, '0');
    }

}
