package exercici2_v2;

public class DeptCienciaProductor_v2 extends Thread {

    private MagatzemCombustible_v2 magatzemDeCombustible;

    public DeptCienciaProductor_v2(MagatzemCombustible_v2 magatzemDeCombustible) {
        this.magatzemDeCombustible = magatzemDeCombustible;
    }

    @Override
    public void run() {
        int i = 0;
        System.out.println("1111 - DeptCienciaProductor.INICI");
        while (i < 20) {
            int numContenidorTmp = magatzemDeCombustible.numContenidorsAlMagatzem();
            if ( numContenidorTmp < 10) {
                System.out.println("1111.1 - DeptCienciaProductor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeCombustible.posiconsEnMagatzem.toString());
                magatzemDeCombustible.produirContenidorDeCombustible();
                System.out.println("1111.2 - DeptCienciaProductor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeCombustible.posiconsEnMagatzem.toString());
                i++;
            }
        }
        System.out.println("1111 - DeptCienciaProductor.FI");
    }
}
