package exercici2_v2;

import Llibreries.Pantalles.MenuConstructorPantalla;

import java.io.IOException;
import java.util.Scanner;

public class PAU487 {

    public static void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("\nToca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }

    public static void menuPAU487() throws IOException  {

        String opcio;
        Scanner sc = new Scanner(System.in);
        StringBuilder menu = new StringBuilder("");

        do {
        	menu.delete(0, menu.length());

            menu.append(System.getProperty("line.separator"));
            menu.append("PAU-487 ");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            menu.append("1. Threads implements Runnable");
            menu.append(System.getProperty("line.separator"));
            menu.append("2. Threads extends Thread (I)");
            menu.append(System.getProperty("line.separator"));
            menu.append("3. Threads extends Thread (II)");
            menu.append(System.getProperty("line.separator"));
            menu.append("10. ");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            menu.append("11. ");
            menu.append(System.getProperty("line.separator"));
            menu.append("12. ");
            menu.append(System.getProperty("line.separator"));
            menu.append("21. ");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            menu.append("50. Tancar el sistema");
            menu.append(System.getProperty("line.separator"));


            System.out.print(MenuConstructorPantalla.constructorPantalla(menu));
            
            opcio = sc.next();
            
            switch (opcio) {
                case "1":

                    break;
                case "2":

                    break;
                case "3":
                    try {
                        Exercici_2_v2.inicialitzarPrograma();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    bloquejarPantalla();
                    break;
                case "50":
                    break; 
                default:
                    System.out.println("COMANDA NO RECONEGUDA");
            }  
        } while (!opcio.equals("50"));
    }
    
}
