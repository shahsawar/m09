package exercici2_v1;

public class Exercici_2_v1 {

    public static void inicialitzarPrograma() throws InterruptedException {

        MagatzemCombustible_v1 magatzemCombustibleV1 = new MagatzemCombustible_v1();
        DeptCienciaProductor_v1 deptCienciaProductorV1 = new DeptCienciaProductor_v1(magatzemCombustibleV1);
        DeptEnginyeriaConsumidor_v1 enginyeriaConsumidorV1 = new DeptEnginyeriaConsumidor_v1(magatzemCombustibleV1);

        System.out.println("Exercici_2.inicialitzarPrograma() - INICI");
        System.out.println("Exercici_2.magatzemDeCombustible.posicionsEnMagatzem = " + new String(magatzemCombustibleV1.getPosiconsEnMagatzem()));

        deptCienciaProductorV1.start();
        enginyeriaConsumidorV1.start();

        deptCienciaProductorV1.join();
        enginyeriaConsumidorV1.join();

        System.out.println("Exercici_2.magatzemDeCombustible.posicionsEnMagatzem = " + new String(magatzemCombustibleV1.getPosiconsEnMagatzem()));
        System.out.println("Exercici_2.inicialitzarPrograma() - FI");

    }
}
