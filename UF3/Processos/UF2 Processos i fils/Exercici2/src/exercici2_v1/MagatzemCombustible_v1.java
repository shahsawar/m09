package exercici2_v1;

public class MagatzemCombustible_v1 {

    private char[] posiconsEnMagatzem = {'0', '0', '0', '0', '0', '0', '0', '0', '0', '0'};
    int posicio = -1;

    public MagatzemCombustible_v1() {
    }

    /*
    public synchronized int numContenidorsAlMagatzemV2() {
        int numContenidors;
        numContenidors = posicio + 1;
        return numContenidors;
    }*/

    public synchronized int numContenidorsAlMagatzem() {

        int numContenidorTmp = 0;
        for (char mtmp : this.posiconsEnMagatzem) {
            if (mtmp == '1')
                numContenidorTmp += 1;
        }
        return numContenidorTmp;
    }

    public void produirContenidorDeCombustible() {

        for (int i = 0; i < posiconsEnMagatzem.length; i++) {
            if (posiconsEnMagatzem[i] == '0') {
                posiconsEnMagatzem[i] = '1';
                break;
            }
        }
        System.out.println(posiconsEnMagatzem);
    }

    public void consumirContenidorDeCombustible() {

        for (int i = 0; i < posiconsEnMagatzem.length; i++) {
            if (posiconsEnMagatzem[i] == '1') {
                posiconsEnMagatzem[i] = '0';
                break;
            }
        }
        System.out.println(posiconsEnMagatzem);
    }


    public char[] getPosiconsEnMagatzem() {
        return posiconsEnMagatzem;
    }

    public void setPosiconsEnMagatzem(char[] posiconsEnMagatzem) {
        this.posiconsEnMagatzem = posiconsEnMagatzem;
    }
}
