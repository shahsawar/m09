package exercici2_v1;

public class DeptCienciaProductor_v1 extends Thread {

    private MagatzemCombustible_v1 magatzemDeCombustible;

    public DeptCienciaProductor_v1(MagatzemCombustible_v1 magatzemDeCombustible) {
        this.magatzemDeCombustible = magatzemDeCombustible;
    }

    @Override
    public void run() {
        int i = 0;
        System.out.println("1111 - DeptCienciaProductor.INICI");
        while (i < 20) {
            int numContenidorTmp = magatzemDeCombustible.numContenidorsAlMagatzem();
            if ( numContenidorTmp < 10) {
                System.out.println("1111.1 - DeptCienciaProductor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeCombustible.getPosiconsEnMagatzem().toString());
                magatzemDeCombustible.produirContenidorDeCombustible();
                System.out.println("1111.2 - DeptCienciaProductor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeCombustible.getPosiconsEnMagatzem().toString());
                i++;
            }
        }
        System.out.println("1111 - DeptCienciaProductor.FI");
    }
}
