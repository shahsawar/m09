package exercici2_v1;

public class DeptEnginyeriaConsumidor_v1 extends Thread {

    MagatzemCombustible_v1 magatzemDeConsumidor = new MagatzemCombustible_v1();

    public DeptEnginyeriaConsumidor_v1(MagatzemCombustible_v1 magatzemDeConsumidor) {
        this.magatzemDeConsumidor = magatzemDeConsumidor;
    }

    @Override
    public void run() {
        int i = 0;
        System.out.println("2222 - DeptEnginyeriaConsumidor.INICI");

        i = 0;
        while (i < 13) {
            int numContenidorTmp = magatzemDeConsumidor.numContenidorsAlMagatzem();
            if (magatzemDeConsumidor.numContenidorsAlMagatzem() > 0) {
                System.out.println("2222.1 - [" + i + "] - DeptEnginyeriaConsumidor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeConsumidor.getPosiconsEnMagatzem().toString());
                magatzemDeConsumidor.consumirContenidorDeCombustible();
                System.out.println("2222.2 - [" + i + "] - DeptEnginyeriaConsumidor.run(), numContenidorsAlMagatzem() = " + numContenidorTmp + ", " + magatzemDeConsumidor.getPosiconsEnMagatzem().toString());
                i++;
            }
        }
        System.out.println("2222 - DeptEnginyeriaConsumidor.FI");
    }
}
